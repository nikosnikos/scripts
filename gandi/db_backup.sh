#!/bin/sh

# Script de backup de la base de données récupéré ici : http://groups.gandi.net/fr/topic/gandi.fr.hebergement.simple/32364

# daily, weekly ou monthly => la fréquence du backup
type=$1

PATH=/usr/sbin/:/usr/bin/:/sbin/:/bin/
export PATH
cd /srv/data/web/includes/backup/$type || exit

# Le nombre de sauvegardes qu'on garde
repeat=4
case "$type" in
'daily') repeat=3  
    ;;
'weekly') repeat=3 
    ;;
'monthly')  repeat=6
    ;;
*)  echo "La fréquence $type n'est pas reconnue"
    exit
    ;;
esac

set -o noclobber
#set -o pipefail
#ulimit -f 20480
renice 10 "$$" > /dev/null
# on garde les 4 derniers backups
output=$(
  exec 2>&1

  echo ""
  
  echo "Sauvegarde des bases de données"
  date '+%F %T %z'
  echo ""
  
  uptime
  echo ""
  
  df -h . 2> /dev/null
  echo ""
  
  set -x
  echo "Réorganisation des e�pertoires"
  rm -rf db-backup-$repeat
  a=$repeat
  while [ $a -gt 0 ]
  do
    b=`expr $a - 1`
    echo "mv db-backup-$b db-backup-$a"
    mv db-backup-$b db-backup-$a
    a=$b
  done

  mkdir db-backup-0 || exit
  cd db-backup-0 || exit

  mysql -u backup -pvAtURCv6RWETjah7 -ss -e 'show databases' |
    while IFS= read -r db; do
      exec 3> "$db.dump.sql.gz.new" || exit
      mysqldump -u backup -pvAtURCv6RWETjah7 --skip-lock-tables --databases --add-drop-database --skip-extended-insert --skip-comments "$db" |
	gzip -3 >&3 && mv -f "$db.dump.sql.gz.new" "$db.dump.sql.gz" || {
	  rm -f "$db.dump.sql.gz.new"
	  exit 1
	}
    done
) || php /srv/data/web/includes/scripts/db_notify.php 

echo "$output"
echo "$output" > /srv/data/web/includes/backup/$type/db-backup-0/README
