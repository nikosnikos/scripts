#!/bin/bash

echo "Modification du domaine dans la base"

local_db_pass="SeL6uZKVTnLQys3R"
db_name="atdqm"
db_user="atdqm"
search_domain_regexp='#(www\.)(atd-(fourthworld|quartmonde|cuartomundo).org)#'
replace_domain_regexp='$2'

# Le script est récupéré ici : https://interconnectit.com/products/search-and-replace-for-wordpress-databases/ et ici https://github.com/interconnectit/Search-Replace-DB
# Met le bon domaine
echo "php Search-Replace-DB-master/srdb.cli.php -p $local_db_pass -h localhost -n $db_name -u $db_user -s \"$search_domain_regexp\" -r \"$replace_domain_regexp\" -g -z"
php Search-Replace-DB-master/srdb.cli.php -p $local_db_pass -h localhost -n $db_name -u $db_user -s "$search_domain_regexp" -r "$replace_domain_regexp" -g
echo "OK"
