# README #

Cette repo archive et versionne les différents scripts que j'utilise. Elle me permet d'avoir une sauvegarde et de gérer les versions.

# Informations #

Voici une listes des répertoires et de leur contenu :

- backup/ : Les scripts qui ont été utilisé pour faire des backups par hubic
- ccfd_prod/ : Les scripts utilisés dans les cron sur le serveur de prod du ccfd
- ccfd_dev/ : Les scripts utilisés sur le serveur de dev du ccfd notamment pour la réplication des sites mirroir en sites de test.
- gandi/ : Les scripts utilisés sur les instances simple hosting de gandi, notamment pour la sauvegarde de la base.
- install-serveur/ : Les scripts qui ont servi à installer les serveurs du ccfd
- mails/ : Les scripts communicant avec bluemind permettant notamment d'envoyer des mails à tout le monde.
- utils/ : Les scripts pour un usage interne et local.
