#!/bin/bash
# On efface les anciennes sauvegarde
USERNAME=ATDdump
PASSWORD=
DIR=/root/backups

#si pas de pwd pour le user dump
if [ ! -n "$PASSWORD" ]; then
  echo "veuillez mettre un password dans le script"
  exit 1
fi

#si le repertoire n'existe pas on le créé
if [ ! -d $DIR ]
then
    mkdir -p $DIR
fi

# On efface les anciennes sauvegarde
rm -f $DIR/*

# Sauvegarde de toute la base
#mysqldump -A > $DIR/touteslesbases.sql -u$USERNAME -p$PASSWORD

# Sauvegarde base par base
for i in `echo "show databases;"|mysql -u$USERNAME -p$PASSWORD|grep -vE "(Database|.*schema)"`
do  mysqldump --databases $i > $DIR/$i.sql -u$USERNAME -p$PASSWORD
done

