#!/bin/bash
######################################################################
# Virtualhost script                                                 #
# Easily add/remove domains or subdomains                            #
# Configures logrotate and PHP5-FPM                                  #
######################################################################

source ../install-server/options.conf

# Seconds to wait before removing a domain/virtualhost
REMOVE_DOMAIN_TIMER=10

# Check domain to see if it contains invalid characters. Option = yes|no.
DOMAIN_CHECK_VALIDITY="yes"

#### First initialize some static variables ####

# Logrotate Postrotate for Nginx
# From options.conf, nginx = 1, apache = 2
if [ $WEBSERVER -eq 1 ]; then
    POSTROTATE_CMD='[ ! -f /var/run/nginx.pid ] || kill -USR1 `cat /var/run/nginx.pid`'
else
    POSTROTATE_CMD='/etc/init.d/apache2 reload > /dev/null'
fi

#### Functions Begin ####

function initialize_variables {

    # Initialize variables based on user input. For add/rem functions displayed by the menu
    DOMAIN_PATH="$WEB_PATH/$DOMAIN"
    DOMAIN_FILES="$DOMAIN_PATH/htdoc"
    DOMAIN_LOGS="$DOMAIN_PATH/logs"
    #GIT_PATH="$WEB_PATH/$DOMAIN_OWNER/repos/$DOMAIN.git"

    # From options.conf, nginx = 1, apache = 2
    if [ $WEBSERVER -eq 1 ]; then
        DOMAIN_CONFIG_PATH="/etc/nginx/sites-available/$DOMAIN"
        DOMAIN_ENABLED_PATH="/etc/nginx/sites-enabled/$DOMAIN"
        LOG_PATH="/var/log/nginx/$DOMAIN"
        LOGROTATE_FILE="nginx-$DOMAIN"
    else
        DOMAIN_CONFIG_PATH="/etc/apache2/sites-available/$DOMAIN"
        DOMAIN_ENABLED_PATH="/etc/apache2/sites-enabled/$DOMAIN"
        LOG_PATH="/var/log/apache2/$DOMAIN"
        LOGROTATE_FILE="apache2-$DOMAIN"
    fi

    # Awstats command to be placed in logrotate file
    if [ $AWSTATS_ENABLE = 'yes' ]; then
        AWSTATS_CMD="/usr/share/awstats/tools/awstats_buildstaticpages.pl -update -config=$DOMAIN -dir=$WEB_PATH/default/awstats -awstatsprog=/usr/lib/cgi-bin/awstats.pl > /dev/null"
    else
        AWSTATS_CMD=""
    fi

}

function reload_webserver {

    # From options.conf, nginx = 1, apache = 2
    if [ $WEBSERVER -eq 1 ]; then
        service nginx reload
    else
        apache2ctl graceful
    fi

} # End function reload_webserver

function add_domain {

    # Create public_html and log directories for domain
    mkdir -p $DOMAIN_FILES
    mkdir -p $DOMAIN_LOGS
    mkdir $DOMAIN_PATH/.ssh
    touch $DOMAIN_PATH/.ssh/authorized_keys

    # On met les logs dans /var/log
    mkdir $LOG_PATH
    touch $LOG_PATH/{access.log,error.log}
    # Et on ouvre un accès à ces logs dans le répertoire du domaine par mount (et non ln -s) pour marcher en environnement chrooté
    mount --bind $LOG_PATH $DOMAIN_LOGS
    echo "$LOG_PATH $DOMAIN_LOGS none bind 0 0" >> /etc/fstab

    cat > $DOMAIN_FILES/index.php <<EOF
<?php
echo ('Bienvenue sur le site $DOMAIN !!');
?>
EOF

    cp ./config/favicon.ico $DOMAIN_FILES/

    echo "Création du user et groupe $DOMAIN_OWNER utilisé pour le FTP et les sites web"
    groupadd $DOMAIN_OWNER
    useradd -g $DOMAIN_OWNER -d $DOMAIN_PATH -s /usr/sbin/nologin $DOMAIN_OWNER

    # Ajoute les users dans le groupe pour tout interdire à other
    adduser www-data $DOMAIN_OWNER
    adduser $SERVER_ADMIN_USER $DOMAIN_OWNER

    # Set permissions
    chown $DOMAIN_OWNER:$DOMAIN_OWNER $DOMAIN_PATH
    chown -R $DOMAIN_OWNER:$DOMAIN_OWNER $DOMAIN_PATH
    chmod 750 $DOMAIN_PATH
    chmod -R 750 $DOMAIN_PATH
    chmod 640 $DOMAIN_FILES/*
    chmod 640 $DOMAIN_PATH/.ssh/*

    # Pour que les logs soient accessibles par le groupe adm
    chown -R $DOMAIN_OWNER:adm $DOMAIN_LOGS
    chmod 640 $DOMAIN_LOGS/*

    service php5-fpm stop

    # Copy over FPM template for this Linux user if it doesn't exist
    cat > /etc/php5/fpm/pool.d/$DOMAIN_OWNER.conf <<EOF
[${DOMAIN_OWNER}]
user = $DOMAIN_OWNER
group = $DOMAIN_OWNER

listen = /var/run/php5-fpm-${DOMAIN_OWNER}.sock

;listen.owner = $DOMAIN_OWNER
;listen.group = $DOMAIN_OWNER
listen.owner = www-data
listen.group = www-data
listen.mode = 0660

pm = dynamic

pm.max_children = ${FPM_MAX_CHILDREN}
pm.start_servers = ${FPM_START_SERVERS}
pm.min_spare_servers = ${FPM_MIN_SPARE_SERVERS}
pm.max_spare_servers = ${FPM_MAX_SPARE_SERVERS}
pm.max_requests = ${FPM_MAX_REQUESTS}

chdir = /
EOF

    service php5-fpm start
    sleep 1
    service php5-fpm restart
    sleep 1

    echo "On vérifie que PHP-FPM tourne bien pour $DOMAIN_OWNER :"
    ps -ef | grep php-fpm | grep $DOMAIN_OWNER
    if [  $? -eq 1  ]; then
        echo -e "\033[31;1mERROR: PHP-FPM ne tourne pas pour le user $DOMAIN_OWNER. Revoir la configuration de /etc/php5/fpm/pool.d/$DOMAIN_OWNER.conf\033[0m"
    fi

    # Virtualhost entry
    # From options.conf, nginx = 1, apache = 2
    if [ $WEBSERVER -eq 1 ]; then
        # Nginx webserver. Use Nginx vHost config
        cat > $DOMAIN_CONFIG_PATH <<EOF
server {
        listen 80;
        #listen [::]:80 default ipv6only=on;

        server_name www.$DOMAIN $DOMAIN;
        root $DOMAIN_PATH/public_html;
        access_log $DOMAIN_LOGS/access.log;
        error_log $DOMAIN_LOGS/error.log;

        index index.php index.html index.htm;
        error_page 404 /404.html;

        location / {
            try_files \$uri \$uri/ /index.php?\$args;
        }

        # Pass PHP scripts to PHP-FPM
        location ~ \.php$ {
            try_files \$uri =403;
            fastcgi_pass unix:/var/run/php5-fpm-$DOMAIN_OWNER.sock;
            include fastcgi_params;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
        }

        # Enable browser cache for CSS / JS
        location ~* \.(?:css|js)$ {
            expires 2d;
            add_header Pragma "public";
            add_header Cache-Control "public";
            add_header Vary "Accept-Encoding";
        }

        # Enable browser cache for static files
        location ~* \.(?:ico|jpg|jpeg|gif|png|bmp|webp|tiff|svg|svgz|pdf|mp3|flac|ogg|mid|midi|wav|mp4|webm|mkv|ogv|wmv|eot|otf|woff|ttf|rss|atom|zip|7z|tgz|gz|rar|bz2|tar|exe|doc|docx|xls|xlsx|ppt|pptx|rtf|odt|ods|odp)$ {
            expires 5d;
            add_header Pragma "public";
            add_header Cache-Control "public";
        }

        # Deny access to hidden files
        location ~ (^|/)\. {
            deny all;
        }

        # Prevent logging of favicon and robot request errors
        location = /favicon.ico { log_not_found off; access_log off; }
        location = /robots.txt  { log_not_found off; access_log off; }
}


server {
        listen 443;
        server_name www.$DOMAIN $DOMAIN;
        root $DOMAIN_PATH/public_html;
        access_log $DOMAIN_LOGS/access.log;
        error_log $DOMAIN_LOGS/error.log;

        index index.php index.html index.htm;
        error_page 404 /404.html;

        ssl on;
        ssl_certificate /etc/ssl/localcerts/webserver.pem;
        ssl_certificate_key /etc/ssl/localcerts/webserver.key;

        ssl_session_timeout 5m;

        ssl_protocols SSLv2 SSLv3 TLSv1;
        ssl_ciphers HIGH:!aNULL:!MD5;
        ssl_prefer_server_ciphers on;

        location / {
            try_files \$uri \$uri/ /index.php?\$args;
        }

        location ~ \.php$ {
            try_files \$uri =403;
            fastcgi_pass unix:/var/run/php5-fpm-$DOMAIN_OWNER.sock;
            include fastcgi_params;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
        }

        # Enable browser cache for CSS / JS
        location ~* \.(?:css|js)$ {
            expires 2d;
            add_header Pragma "public";
            add_header Cache-Control "public";
            add_header Vary "Accept-Encoding";
        }

        # Enable browser cache for static files
        location ~* \.(?:ico|jpg|jpeg|gif|png|bmp|webp|tiff|svg|svgz|pdf|mp3|flac|ogg|mid|midi|wav|mp4|webm|mkv|ogv|wmv|eot|otf|woff|ttf|rss|atom|zip|7z|tgz|gz|rar|bz2|tar|exe|doc|docx|xls|xlsx|ppt|pptx|rtf|odt|ods|odp)$ {
            expires 5d;
            add_header Pragma "public";
            add_header Cache-Control "public";
        }

        # Deny access to hidden files
        location ~ (^|/)\. {
            deny all;
        }

        # Prevent logging of favicon and robot request errors
        location = /favicon.ico { log_not_found off; access_log off; }
        location = /robots.txt  { log_not_found off; access_log off; }
}
EOF
    else # Use Apache vHost config

        # Si c'est un domaine atd-quartmonde.org On a le certificat signé.
        if [[ "$DOMAIN" = *"atd-quartmonde.org" ]]; then
          CERT_FILES=$(cat <<EOF
    SSLCertificateFile /etc/ssl/certs/atd-quartmonde.org.crt
    SSLCertificateKeyFile /etc/ssl/private/atd-quartmonde.org.key
    SSLCACertificateFile /etc/ssl/certs/GandiStandardSSLCA2.pem
    SSLVerifyClient None
EOF
)
        # Sinon on utilise le certificat généré localement
        else
          CERT_FILES=$(cat <<EOF
    SSLCertificateFile    /etc/ssl/localcerts/webserver.pem
    SSLCertificateKeyFile /etc/ssl/localcerts/webserver.key
EOF
)
        fi

        cat > $DOMAIN_CONFIG_PATH <<EOF
<VirtualHost *:80>

    ServerName $DOMAIN
    ServerAlias www.$DOMAIN
    ServerAdmin $ADMIN_EMAIL
    DocumentRoot $DOMAIN_FILES/
    ErrorLog $DOMAIN_LOGS/error.log
    CustomLog $DOMAIN_LOGS/access.log combined

    Alias /fcgi-bin/php5-fpm /fcgi-bin-php5-fpm-$DOMAIN_OWNER
    FastCgiExternalServer /fcgi-bin-php5-fpm-$DOMAIN_OWNER  -idle-timeout 120 -socket /var/run/php5-fpm-$DOMAIN_OWNER.sock -pass-header Authorization
</VirtualHost>


<IfModule mod_ssl.c>
<VirtualHost *:443>

    ServerName $DOMAIN
    ServerAlias www.$DOMAIN
    ServerAdmin $ADMIN_EMAIL
    DocumentRoot $DOMAIN_FILES/
    ErrorLog $DOMAIN_LOGS/error.log
    CustomLog $DOMAIN_LOGS/access.log combined

    # With PHP5-FPM, you need to create another PHP5-FPM pool for SSL connections
    # Adding the same fastcgiexternalserver line here will result in an error
    Alias /fcgi-bin/php5-fpm /fcgi-bin-php5-fpm-$DOMAIN_OWNER

    SSLEngine on
    $CERT_FILES

    <FilesMatch "\.(cgi|shtml|phtml|php)$">
        SSLOptions +StdEnvVars
    </FilesMatch>

    BrowserMatch "MSIE [2-6]" nokeepalive ssl-unclean-shutdown downgrade-1.0 force-response-1.0
    BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown
</VirtualHost>
</IfModule>
EOF
    fi # End if $WEBSERVER -eq 1

    if [ $AWSTATS_ENABLE = 'yes' ]; then
        # Configure Awstats for domain
        sed '1d' /etc/awstats/awstats.conf.template > /etc/awstats/awstats.$DOMAIN.conf
        echo "" >> /etc/awstats/awstats.$DOMAIN.conf
        echo "" >> /etc/awstats/awstats.$DOMAIN.conf
        echo "# Configuration spécifique pour $DOMAIN." >> /etc/awstats/awstats.$DOMAIN.conf
        echo "SiteDomain=\"${DOMAIN}\"" >> /etc/awstats/awstats.$DOMAIN.conf
        echo "# Other alias, basically other domain/subdomain that's the same as the domain above" >> /etc/awstats/awstats.$DOMAIN.conf
        echo "HostAliases=\"www.${DOMAIN}\"" >> /etc/awstats/awstats.$DOMAIN.conf
        echo "LogFile=\"$DOMAIN_LOGS/access.log\"" >> /etc/awstats/awstats.$DOMAIN.conf
        echo "DirData=\"$WEB_PATH/default/awstats/${DOMAIN}.data\"" >> /etc/awstats/awstats.$DOMAIN.conf
        mkdir -p $WEB_PATH/default/awstats/${DOMAIN}.data
    fi

    # Add new logrotate entry for domain
    cat > /etc/logrotate.d/$LOGROTATE_FILE <<EOF
$LOG_PATH/*.log {
    weekly
    missingok
    rotate 52
    compress
    delaycompress
    notifempty
    create 0640 $DOMAIN_OWNER adm
    sharedscripts
    prerotate
        $AWSTATS_CMD
    endscript
    postrotate
        $POSTROTATE_CMD
    endscript
}
EOF
    # Enable domain from sites-available to sites-enabled
    a2ensite $DOMAIN

    # GIT
    if [ $GIT_ENABLE = 'yes' ]; then
        mkdir -p $GIT_PATH
        cd $GIT_PATH
        git init --bare
        cat > hooks/post-receive <<EOF
#!/bin/sh
GIT_WORK_TREE=$DOMAIN_PATH git checkout -f
EOF
        chmod +x hooks/post-receive
        cd - &> /dev/null

        # Set permissions
        chown -R $DOMAIN_OWNER:$DOMAIN_OWNER $GIT_PATH
        echo -e "\033[35;1mSuccesfully Created git repository \033[0m"
        echo -e "\033[35;1mgit remote add web ssh://$DOMAIN_OWNER@$HOSTNAME_FQDN:$SSHD_PORT/$GIT_PATH \033[0m"
    fi

    echo -e "\033[32;1mAjoute une exception dans tripwire pour ne pas checker les logs qui à priori vont bouger tous les jours\033[0m"
    echo -e  "\033[35;1mNOTICE: Préparer les clés tripwire pour valider les modifs !\033[0m"
    sed -i 's#\(^\s*\)\(/var/www\)\(\s.*$\)#\1\2\3\n\1!'$DOMAIN_PATH'/logs ;#' /etc/tripwire/twpol.txt
    tripwire --update-policy --secure-mode low /etc/tripwire/twpol.txt
    echo "Réinitialise tripwire avec la nouvelle configuration, pour éviter un mail de trop !"
    tripwire --check --interactive
} # End function add_domain


function remove_domain {

    echo -e "\033[31;1mWARNING: This will permanently delete everything related to $DOMAIN\033[0m"
    echo -e "\033[31mIf you wish to stop it, press \033[1mCTRL+C\033[0m \033[31mto abort.\033[0m"
    sleep $REMOVE_DOMAIN_TIMER

    # First disable domain and reload webserver
    echo -e "* Disabling domain: \033[1m$DOMAIN\033[0m"
    sleep 1
    rm -rf $DOMAIN_ENABLED_PATH
    reload_webserver

    # Then delete all files and config files
    if [ $AWSTATS_ENABLE = 'yes' ]; then
        echo -e "* Removing awstats config: \033[1m/etc/awstats/awstats.$DOMAIN.conf\033[0m"
        sleep 1
        rm -rf /etc/awstats/awstats.$DOMAIN.conf
    fi

    WP_CONFIG="$DOMAIN_FILES/wp-config.php"
    [ -f $WP_CONFIG ]
    HAVE_WP_CONFIG=$?
    if [ $HAVE_WP_CONFIG -eq 0 ]; then
      DB_NAME=$(grep "define\s*(\s*'DB_NAME'" $WP_CONFIG | sed 's/^\s*define(\s*'"'"'DB_NAME'"'"'\s*,\s*'"'"'\(\w*\)'"'"'\s*)\s*;\s*$/\1/')
      DB_USER=$(grep "define\s*(\s*'DB_USER'" $WP_CONFIG | sed 's/^\s*define(\s*'"'"'DB_USER'"'"'\s*,\s*'"'"'\(\w*\)'"'"'\s*)\s*;\s*$/\1/')
    fi

    echo -e "* Removing domain files: \033[1m$DOMAIN_PATH\033[0m"
    sleep 1
    umount $DOMAIN_LOGS
    LOG_PATH_ESCAPED=$(echo "$LOG_PATH" | sed -e 's/\([[\/.*]\|\]\)/\\&/g')
    sed -i '/'${LOG_PATH_ESCAPED}'/d' /etc/fstab
    rm -rf $DOMAIN_PATH
    echo -e "\033[32;1mSupprime la config du domaine dans tripwire\033[0m"
    echo -e  "\033[35;1mNOTICE: Préparer les clés tripwire pour valider les modifs !\033[0m"
    grep -v "$DOMAIN_PATH" /etc/tripwire/twpol.txt > /etc/tripwire/twpol.txt.tmp
    mv /etc/tripwire/twpol.txt.tmp /etc/tripwire/twpol.txt
    tripwire --update-policy --secure-mode low /etc/tripwire/twpol.txt
    echo "Réinitialise tripwire avec la nouvelle configuration, pour éviter un mail de trop !"
    tripwire --check --interactive
	
    echo -e "* Removing vhost file: \033[1m$DOMAIN_CONFIG_PATH\033[0m"
    sleep 1
    rm -rf $DOMAIN_CONFIG_PATH

    echo -e "* Removing log files: \033[1m$LOG_PATH\033[0m"
    sleep 1
    rm -rf $LOG_PATH

    echo -e "* Delete user access to \033[1mphpmyadmin\033[0m"
    sleep 1
    htpasswd -D /etc/apache2/users.passwd ${DOMAIN_OWNER}_admin

    echo -e "* Delete user access to \033[1mSFTP\033[0m"
    sleep 1
    ./ftpuser.sh rem ${DOMAIN_OWNER}_admin

    echo -e "* Removing logrotate file: \033[1m/etc/logrotate.d/$LOGROTATE_FILE\033[0m"
    sleep 1
    rm -rf /etc/logrotate.d/$LOGROTATE_FILE

    echo -e "* Removing git repository: \033[1m$GIT_PATH\033[0m"
    sleep 1
    rm -rf $GIT_PATH

    echo -e "* Removing php-fpm file for this user: \033[1m/etc/php5/fpm/pool.d/$DOMAIN_OWNER.conf\033[0m"
    sleep 5
    rm -rf /etc/php5/fpm/pool.d/$DOMAIN_OWNER.conf
    service php5-fpm restart

    echo -e "* \033[1mRemoving USER $DOMAIN_OWNER. Ctrl-C to abort.\033[0m"
    sleep $REMOVE_DOMAIN_TIMER
    service php5-fpm stop
    sleep 1
    # Suppression du user
    deluser $DOMAIN_OWNER
    # Suppression du groupe parce que www-data et l'admin en sont membre, il ne supprime donc pas par défaut.
    delgroup $DOMAIN_OWNER

    # php5-fpm a l'air capricieux parfois sur le redémarrage on le redémarre donc plusierus fois
    service php5-fpm start
    sleep 1
    service php5-fpm restart

    if [ $HAVE_WP_CONFIG -eq 0 ]; then
      echo -e "* \033[1mRemoving DATABASE NAME $DB_NAME and USER $DB_USER . Ctrl-C to abort.\033[0m"
      sleep $REMOVE_DOMAIN_TIMER
      mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "DROP USER '$DB_USER'@'localhost'; DROP DATABASE $DB_NAME;"
      sleep 1
    fi

} # End function remove_domain

function check_user_exists {
  egrep -i "^${DOMAIN_OWNER}:" /etc/passwd
  return $?
} # End function check_user_exists


function check_domain_valid {

    # Check if the domain entered is actually valid as a domain name
    # NOTE: to disable, set "DOMAIN_CHECK_VALIDITY" to "no" at the start of this script
    if [ "$DOMAIN_CHECK_VALIDITY" = "yes" ]; then
        if [[ "$DOMAIN" =~ [\~\!\@\#\$\%\^\&\*\(\)\_\+\=\{\}\|\\\;\:\'\"\<\>\?\,\/\[\]] ]]; then
            echo -e "\033[35;1mERROR: Domain check failed. Please enter a valid domain.\033[0m"
            echo -e "\033[35;1mERROR: If you are certain this domain is valid, then disable domain checking option at the beginning of the script.\033[0m"
            return 1
        else
            return 0
        fi
    else
    # If $DOMAIN_CHECK_VALIDITY is "no", simply exit
        return 0
    fi

} # End function check_domain_valid

#### Main program begins ####

# Show Menu
if [ ! -n "$1" ]; then
    echo ""
    echo -e "\033[35;1mSelect from the options below to use this script:- \033[0m"
    echo -n  "$0"
    echo -ne "\033[36m add Domain.tld user email\033[0m"
    echo     " - Add specified domain to $WEB_PATH. AWStats(optional) and log rotation will be configured. An email will be sent tot email with infos."

    echo -n  "$0"
    echo -ne "\033[36m rem Domain.tld\033[0m"
    echo     " - Remove everything for Domain.tld including stats and public_html. If necessary, backup domain files before executing!"

    echo ""
    exit 0
fi
# End Show Menu

case $1 in
add)
    # Add domain for user
    # Check for required parameters
    if [ $# -ne 4 ]; then
        echo -e "\033[31;1mERROR: Please enter the required parameters.\033[0m"
        exit 1
    fi

    # Set up variables
    DOMAIN=$2
    DOMAIN_OWNER=$3
    DOMAIN_EMAIL=$4

    initialize_variables

    # Check if it is a new user
    check_user_exists
    if [  $? -eq 0  ]; then
      echo -e "\033[35;1mERROR: User $DOMAIN_OWNER already exists. Please enter a new user or delete ${DOMAIN_OWNER}.\033[0m"
      exit 1
    fi

    # Check if domain is valid
    check_domain_valid
    if [ $? -ne 0 ]; then
        exit 1
    fi

    # Check if domain config files exist
    if [ -e "$DOMAIN_CONFIG_PATH" ] || [ -e "$DOMAIN_PATH" ]; then
        echo -e "\033[31;1mERROR: $DOMAIN_CONFIG_PATH or $DOMAIN_PATH already exists. Please remove before proceeding.\033[0m"
        exit 1
    fi

    add_domain
    reload_webserver

    echo -e "\033[32;1mCréation de l'accès SFTP!\033[0m"
    ./ftpuser.sh add ${DOMAIN_OWNER}_admin $DOMAIN

    echo -e "\033[32mAccès à phpmyadmin (mettre le même mot de passe que le SFTP).\033[0m"
    htpasswd /etc/apache2/users.passwd ${DOMAIN_OWNER}_admin
    if [  $? -ne 0  ]; then
      echo -e "\033[35;1m L'utilisateur n'a pas été ajouté, vous avez une deuxième chance ;) \033[0m"
      echo "Et si ça ne marche toujours pas vous pouvez tenter à la main : htpasswd /etc/apache2/users.passwd ${DOMAIN_OWNER}_admin"
      htpasswd /etc/apache2/users.passwd ${DOMAIN_OWNER}_admin
    fi

    echo ""
    echo -n "Do you want to install wordpress [y/n] : "

    read DECISION

    if [[ "$DECISION" = [yY] ]] || [ -z $DECISION ]; then
      . ./wordpress.sh
      WORDPRESS_MESSAGE="

L'accès aux bases de données peut se faire par phpmyadmin :
adresse : https://${HOSTNAME_FQDN}/dbadmin
La première authentification (par fenêtre popup) se fait avec le même utilisateur et mot de passe que pour sftp
La deuxième authentification est avec le user $DB_USER et mdp $DB_USER_PASS.

Il vous reste à compléter l'installation de wordrpress en vous rendant sur http://${DOMAIN}
"
    else
        echo "OK. Pas d'installation de wordpress. Envoie du mail avec les infos..."
    fi

   echo "Bonjour,

Le site \"${DOMAIN}\" vient d'être configuré sur le serveur.
Le nom de domaine peut mettre plusieurs heures à être actif, vous pourrez alors accéder au site ici http://${DOMAIN}

L'accès au serveur se fait par SFTP sur le port $SFTP_PORT
host : $HOSTNAME_FQDN
login : ${DOMAIN_OWNER}_admin
mot de passe dans un prochain mail
$WORDPRESS_MESSAGE

Le Webmestre" | /usr/bin/mail.mailutils -s "Accès au site ${DOMAIN}" -r $ADMIN_EMAIL ${DOMAIN_EMAIL}

    # Note pour se rapeller d'envoyer le mdp de manière un peu plus sécurisée que par le serveur
    echo "Envoyer le mot de passe par mail à ${DOMAIN_EMAIL}" | /usr/bin/mail.mailutils -s "Et le reste..." $ADMIN_EMAIL

    echo -e "\033[35;1mSuccesfully added \"${DOMAIN}\" to user \"${DOMAIN_OWNER}\" \033[0m"
    echo -e "\033[35;1mYou can now upload your site to $DOMAIN_FILES \033[0m"
    echo -e "\033[35;1mIf Varnish cache is enabled, please disable & enable it again to reconfigure this domain. \033[0m"
    echo -e "\033[32;1m /!\ Penser à envoyer le mot de passe par mail !!! /!\ \033[0m"
    ;;

rem)
    # Add domain for user
    # Check for required parameters
    if [ $# -ne 2 ]; then
        echo -e "\033[31;1mERROR: Please enter the required parameters.\033[0m"
        exit 1
    fi

    # Set up variables
    DOMAIN=$2
    DOMAIN_OWNER=$(ls -alF ${WEB_PATH}/${DOMAIN} |grep " \./" | awk '{print $3}')

    initialize_variables

    # Check if domain config files exist
    if [ ! -e "$DOMAIN_CONFIG_PATH" ] || [ ! -e "$DOMAIN_PATH" ] || [ -z $DOMAIN_OWNER ] || [ ! -e "$DOMAIN_LOGS" ] || [ ! -e "$DOMAIN_PATH" ] || [ ! -e "$LOG_PATH" ]; then
        echo -e "\033[31;1mERROR: Il y a des valeurs manquantes pour supprimer le domaine.\033[0m"
        echo "Il est possible par exemple que $DOMAIN_CONFIG_PATH ou $DOMAIN_PATH ou le user '$DOMAIN_OWNER' associé au domaine n'existe pas."
        echo "Si une des valeurs suivantes est à 0, c'est de là que ça vient :"
        [ ! -e $DOMAIN_CONFIG_PATH ]
        echo "DOMAIN_CONFIG_PATH = $?"
        [ -z $DOMAIN_OWNER ]
        echo "DOMAIN_OWNER = $?"
        [ ! -e "$DOMAIN_LOGS" ]
        echo "DOMAIN_LOGS = $?"
        [ ! -e "$LOG_PATH" ]
        echo "LOG_PATH = $?"
        echo -e " - \033[34;1mNOTE:\033[0m \033[34mThere may be files left over. Please check manually to ensure everything is deleted.\033[0m"
      echo "Voici les commandes à lancer à la main en root :"
      LOG_PATH_ESCAPED=$(echo "$LOG_PATH" | sed -e 's/\([[\/.*]\|\]\)/\\&/g')
      echo "
rm -rf $DOMAIN_ENABLED_PATH
rm -rf /etc/awstats/awstats.$DOMAIN.conf
umount $DOMAIN_LOGS
sed -i '/'${LOG_PATH_ESCAPED}'/d' /etc/fstab
rm -rf $DOMAIN_PATH
rm -rf $DOMAIN_CONFIG_PATH
rm -rf /etc/logrotate.d/$LOGROTATE_FILE
rm -rf $LOG_PATH
rm -rf $GIT_PATH
rm -rf /etc/php5/fpm/pool.d/$DOMAIN_OWNER.conf
deluser $DOMAIN_OWNER
service php5-fpm restart
service apache2 restart
"
        exit 1
    fi

    remove_domain
    ;;

*)
    echo "$1 n'est pas une option valide !"
    exit 1
    ;;
esac
