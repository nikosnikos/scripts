#!/bin/bash
######################################################################
# Easily add/remove FTP users                                        #
######################################################################

source ../install-server/options.conf

FTPASSWD_OPTS="--file /etc/proftpd/ftpd.passwd"
#### Main program begins ####

# Show Menu
if [ ! -n "$1" ]; then
    echo ""
    echo -e "\033[35;1mSelect from the options below to use this script:- \033[0m"
    echo -n  "$0"
    echo -ne "\033[36m add username [dir] \033[0m"
    echo     " - Add specified username with passwd to \"FTP users\" and the subdirectory of $WEB_PATH (optional) the user can access."

    echo -n  "$0"
    echo -ne "\033[36m rem username \033[0m"
    echo     " - Remove specified username from \"FTP users\"."

    echo -n  "$0"
    echo -ne "\033[36m pwd username password \033[0m"
    echo     " - Change the password of the specified username with password from \"FTP users\""

    echo ""
    exit 0
fi
# End Show Menu


case $1 in
add)
    # Add FTP USER
    # Check for required parameters
    if [ $# -ne 2 ] && [ $# -ne 3 ]; then
        echo -e "\033[31;1mERROR: Please enter the required parameters.\033[0m"
        exit 1
    fi

    # Set up variables
    FTPUSER=$2

    if [ $# -eq 3 ]; then
      FTPDIR=${WEB_PATH}/$3
    else
      FTPDIR=$WEB_PATH
    fi

    FTPDIR_OWNER=$(ls -anF $FTPDIR |grep " \./" | awk '{print $3}')
    # Commande fournie par proftpd pour générer le fichier de mot de passe
    ftpasswd --passwd --name $FTPUSER --home $FTPDIR --shell /bin/false --uid $FTPDIR_OWNER $FTPASSWD_OPTS

    echo -e "\033[35;1mSuccesfully added \"${FTPUSER}\" to FTP users \033[0m"
    ;;
rem)
    # Remove FTP USER
    # Check for required parameters
    if [ $# -ne 2 ]; then
        echo -e "\033[31;1mERROR: Please enter the required parameters.\033[0m"
        exit 1
    fi

    FTPUSER=$2

    ftpasswd --passwd --name $FTPUSER $FTPASSWD_OPTS --delete-user

    echo -e "\033[35;1mSuccesfully removed \"${FTPUSER}\" from FTP users \033[0m"
    ;;
pwd)
    # Change user Passwd
    # Check for required parameters
    if [ $# -ne 2 ]; then
        echo -e "\033[31;1mERROR: Please enter the required parameters.\033[0m"
        exit 1
    fi

    FTPUSER=$2

    ftpasswd --passwd --name $FTPUSER $FTPASSWD_OPTS --change-password

    echo -e "\033[35;1mSuccesfully change the password of the FTP user : \"${FTPUSER}\" \033[0m"
    ;;
*)
    echo "$1 n'est pas une option valide !"
    exit 1
    ;;
esac
