# C'est quoi tout ça ?
Le répertoire contient l'ensemble de scripts utilisés pour ajouter un site web sur le serveur.

# Et plus précisément ?
Voici ce que fait chacun des sripts :
- domain.sh : Crée ou supprime les répertoires pour un site web (dans /var/www/nom.du.site.web/), la base de donnée qui va bien, l'acces SFTP, la config apache, le necessaire pour wordpress si on le veut...
- ftpuser.sh : crée ou supprime un espace sftp pour gérer le site web.
- wordpress.sh : Télécharge la dernière version de wordpress et la configure comme il faut.

# Et comment j'utilise tout ça ?
L'utilisation la plus courante est la suivante :
./domain.sh add domain.tld user email 
./domain.sh rem domain.tld
