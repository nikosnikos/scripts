# C'est quoi ici ?
Ce répertoire contient le script `piwik_summary.php` utilisé pour envoyer par mail à certains utilisateur-trices les statistiques Piwik pour un certain nombre de sites web.

# Et comment ça marche ?
Le script s'utilise simplement en faisant : 
`php piwik_summary.php`

Il est lancé par cron dans crontab root toutes les nuits :
`23 03 * * * php /root/scripts/send-analytics/piwik_summary.php`
