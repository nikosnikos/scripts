<?php
require 'phpmailer/PHPMailerAutoload.php';

setlocale(LC_ALL, 'fr_FR');

// Les jours étudiés
$yesterday_timestamp = strtotime("yesterday");
$day_before_yesterday_timestamp = strtotime('-1 day', $yesterday_timestamp  );
$last_week_timestamp = strtotime('-7 day', $yesterday_timestamp  );

$today = strftime("%Y-%m-%d", time());
echo "$today\n";
$yesterday = strftime("%Y-%m-%d", $yesterday_timestamp);
$day_before_yesterday = strftime("%Y-%m-%d", $day_before_yesterday_timestamp);
$last_week = strftime("%Y-%m-%d", $last_week_timestamp);

$yesterday_nice = strftime("%d/%m/%Y", $yesterday_timestamp);
$day_before_yesterday_nice = strftime("%d/%m/%Y", $day_before_yesterday_timestamp);
$last_week_nice = strftime("%d/%m/%Y", $last_week_timestamp);

$message = <<<EOF
<html>
  <head>
      <meta charset="utf-8">
      <meta name="robots" content="noindex,nofollow">
  </head>
  <body style="font-family: dejavusans; color: rgb(13,13,13);line-height: 1.33;">

    <a id="reportTop" rel="noreferrer" target="_blank" href="http://piwik.atd-extranet.org/">
      Voir les stats sur le site Piwik
    </a>

    <h1 style="font-weight:normal; color: rgb(13,13,13); font-size: 24pt;">
        Bilan du $yesterday_nice
    </h1>
    <h2 style="color: rgb(13,13,13); font-size: 16pt; font-weight:normal;">
      Comparaison : $last_week_nice
    </h2>
EOF;

    // this token is used to authenticate your API request.
    // You can get the token on the API page inside your Piwik interface
    $token_auth = '9a5b20b64b181ca993e70e14fd74a998';
$filter = array('nb_uniq_visitors' => 'Visiteurs uniques', 'nb_visits' => 'Visites', 'nb_downloads' => 'Téléchargements', 'nb_pageviews' => 'Pages vues'/*, 'bounce_rate' => 'Taux de rebond', 'avg_time_on_site' => 'Temps passé sur le site'*/);

    $websites = array(63 => 'atd-quartmonde.fr', 2 => 'atd-quartmonde.org', 1 => 'refuserlamisere.org', '67' => 'joseph-wresinski.org', '68' => 'revue-quartmonde.org et editionsquartmonde.org/rqm', '69' => 'zerochomeurdelongueduree.org');

    $sites_datas = array();

    $best_scores = array();
    $worst_scores = array();

    foreach($websites as $id_site => $website) {
      // we call the REST API and request the 100 first keywords for the last month for the idsite=7
      $url = "http://piwik.atd-extranet.org/";
      $url .= "?module=API";
      //$url .= "&method=API.getBulkRequest&urls[0]=method%3dVisitsSummary.get%26idSite%3d$id_site%26date%3d$yesterday%26period%3dday&urls[1]=method%3dVisitsSummary.get%26idSite%3d$id_site%26date%3d$last_week%26period%3dday&urls[2]=method%3dActions.getDownloads%26idSite%3d$id_site%26date%3d$yesterday%26period%3dday&urls[3]=method%3dActions.getDownloads%26idSite%3d$id_site%26date%3d$last_week%26period%3dday";
      $url .= "&method=API.get&idSite=$id_site&period=day&date=last31";
      $url .= "&format=PHP&translateColumnNames=1";
      $url .= "&token_auth=$token_auth";

      $fetched = file_get_contents($url);
      $content = unserialize($fetched);

      /*
       * Content ressemble à ça :
       * Array
        (
          [2016-02-02] => Array
          (
              [nb_uniq_visitors] => 475
              [nb_visits] => 513
              [nb_users] => 0
              [nb_actions] => 1393
              [max_actions] => 70
              [bounce_count] => 321
              [sum_visit_length] => 69529
              [nb_visits_returning] => 67
              [nb_actions_returning] => 320
              [nb_uniq_visitors_returning] => 48
              [nb_users_returning] => 0
              [max_actions_returning] => 70
              [bounce_rate_returning] => 43%
              [nb_actions_per_visit_returning] => 4.8
              [avg_time_on_site_returning] => 348
              [nb_conversions] => 4
              [nb_visits_converted] => 4
              [revenue] => 0
              [conversion_rate] => 0.78%
              [nb_conversions_new_visit] => 3
              [nb_visits_converted_new_visit] => 3
              [revenue_new_visit] => 0
              [conversion_rate_new_visit] => 0.67%
              [nb_conversions_returning_visit] => 1
              [nb_visits_converted_returning_visit] => 1
              [revenue_returning_visit] => 0
              [conversion_rate_returning_visit] => 1.49%
              [nb_pageviews] => 1280
              [nb_uniq_pageviews] => 1001
              [nb_downloads] => 30
              [nb_uniq_downloads] => 26
              [nb_outlinks] => 83
              [nb_uniq_outlinks] => 71
              [nb_searches] => 0
              [nb_keywords] => 0
              [nb_hits_with_time_generation] => 1159
              [avg_time_generation] => 0.917
              [bounce_rate] => 63%
              [nb_actions_per_visit] => 2.7
              [avg_time_on_site] => 136
          )
          ....
        )
      */

      // case error
      if (!$content) {
          print("Error, content fetched = " . $fetched);
      }

      // Calcul des meilleurs et pires scores pour la période.
      $best_scores[$id_site] = $content[$yesterday];
      $worst_scores[$id_site] = $content[$yesterday];
      foreach($content as $day => $day_metrics) {
        if ($day != $today) {
          foreach($day_metrics as $key => $value) {
            if ($key == 'bounce_rate') {
              if ($value <= $best_scores[$id_site][$key]) {
                $best_scores[$id_site][$key] = $value;
              }
              if ($value >= $worst_scores[$id_site][$key]) {
                $worst_scores[$id_site][$key] = $value;
              }
            } else {
              if ($value >= $best_scores[$id_site][$key]) {
                $best_scores[$id_site][$key] = $value;
              }
              if ($value <= $worst_scores[$id_site][$key]) {
                $worst_scores[$id_site][$key] = $value;
              }
            }
          }
        }
      }

      // Récupération des valeurs pour chaque metrique qu'on veut envoyer.
      foreach ($filter as $key => $name) {
        $yesterday_count = (int) $content[$yesterday][$key];
        $last_week_count = (int) $content[$last_week][$key];
        $sites_datas[$id_site][$key]['value'] = number_format($yesterday_count , 0 , ',', ' ') . (($key == 'bounce_rate') ? '&nbsp;%' : '');
        $sites_datas[$id_site][$key]['value_style'] = '';
        if ( $yesterday_count == $best_scores[$id_site][$key] ) {
          $sites_datas[$id_site][$key]['value_style'] = "color: #F545E7; font-weight: bold;";
        } 
        if ( $yesterday_count == $worst_scores[$id_site][$key] ) {
          $sites_datas[$id_site][$key]['value_style'] = "color: #BE8739; font-weight: bold;";
        }
        if ($last_week_count == 0) {
          // Evolution infinie si à 0 la semaine dernière et pas à 0 hier mais bon... On met à 100 c'est plus clair.
          // Et on met à 0 si zero aussi cette semaine.
          $growth = $yesterday_count == 0 ? 0 : 100;
        } else {
          // evolution = (totalhier - totalavanthier) / totalavanthier
          $growth = round(100 * ($yesterday_count - $last_week_count) / $last_week_count, 2);
        }
        // L'évolution par rapport au même jour la semaine dernière
        $sites_datas[$id_site][$key]['growth_by_day'] = (($growth > 0) ? '+' : '') . number_format($growth, 2, ',', ' ');
        $positiv_color = '#008000';
        $negativ_color = '#FF0000';
        if ( $key == 'bounce_rate' ) {
          $sites_datas[$id_site][$key]['growth_by_day_color'] = ($growth>0) ?  $negativ_color : $positiv_color;
        } 
        else {
          $sites_datas[$id_site][$key]['growth_by_day_color'] = ($growth>0) ? $positiv_color : $negativ_color;
        }
      }
    }
    $message .= <<<EOF
    <table style="border-collapse:collapse; margin-left: 5px;">
      <thead style="background-color: rgb(255,255,255); color: rgb(13,13,13); font-size: 11pt; text-transform: uppercase; line-height:2.5em;">
        <th style="font-weight: normal; font-size:10px; text-align:left; padding: 5px;">
          Site Web
        </th>
EOF;
        foreach($filter as $key => $name):
        $message .= <<<EOF
        <th style="font-weight: normal; font-size:10px; text-align:left; padding: 5px;">
          $name
        </th>
EOF;
        endforeach;
        $message .= <<<EOF
      </thead>
      <tbody>
EOF;
        $count = 0;
        foreach($websites as $id_site => $website):
          if ($count % 2 == 0) {
            $bckg = 'background-color: rgb(242,242,242);';
          }
          else {
            $bckg = '';
          }
          $count++;
          $message .= <<<EOF
        <tr style="line-height: 22px;$bckg">
          <td style="font-size: 13px; border-right: 1px solid rgb(217,217,217); padding: 5px;">
            $website
          </td>
EOF;
          foreach($filter as $key => $name):
            $metric = $sites_datas[$id_site][$key]['value'];
            $growth_color = $sites_datas[$id_site][$key]['growth_by_day_color'];
            $growth_by_day = $sites_datas[$id_site][$key]['growth_by_day'];
            $value_style = $sites_datas[$id_site][$key]['value_style'];
            $message .= <<<EOF
          <td style="font-size: 13px; border-right: 1px solid rgb(217,217,217);  padding: 5px;">
            <span style="$value_style">
            $metric
            </span>
            <span style="color: $growth_color">
            ($growth_by_day%)
            </span>
          </td>
EOF;
          endforeach;

        $message .= '</tr>';
        endforeach;
        $message .= <<<EOF
      </tbody>
    </table>
    <br/>
    <p style="color: #F545E7;">en violet : meilleur score sur les 30 derniers jours</p>
    <p style="color: #BE8739;">en ocre : plus faible score sur les 30 derniers jours</p>
EOF;
        /*
      foreach($websites as $id_site => $site_name):
        $message .= <<<EOF
    <p>Liste des meilleurs scores pour les 30 derniers jours pour $site_name</p>
    <ul>
EOF;
        foreach($filter as $key => $name):
          $message .= '<li><b>' . $name  . '</b> : ' .  $best_scores[$id_site][$key] .'</li>';
        endforeach;
    $message .= <<<EOF
    </ul>
    <p>Liste des plus faibles scores pour les 30 derniers jours pour $site_name</p>
    <ul>
EOF;
        foreach($filter as $key => $name):
          $message .= '<li><b>' . $name  . '</b> : ' . $worst_scores[$id_site][$key] .'</li>';
        endforeach;
    $message .= <<<EOF
    </ul>
EOF;
        endforeach; // foreach($websites)
         */
    $message .= <<<EOF
    <a style="text-decoration:none; color: rgb(13,13,13); font-size: 9pt;" href="#reportTop">
        Retour haut de page
    </a>
  </body>
</html>
EOF;

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.atd-quartmonde.org';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'envoi@atd-quartmonde.org';                 // SMTP username
$mail->Password = 'xwmhYRBvdJGC';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->setFrom('webmestre@atd-quartmonde.org', 'Statistiques Piwik');
$mail->addAddress('webmestre@atd-quartmonde.org', 'Webmestre');     // Add a recipient
$mail->addAddress('typhaine@atd-quartmonde.org', 'Typhaine Cornacchiari');
$mail->addAddress('communication@atd-quartmonde.org', 'Communication France');
$mail->addAddress('sarrotjc@free.fr','Jean-Christophe Sarrot');
$mail->addAddress('denis.rochette@atd-quartmonde.org','Denis Rochette');
$mail->addAddress('sophie.marechal@atd-quartmonde.org','Sophie Maréchal');
$mail->addAddress('jeff.steiner@atd-fourthworld.org','Jeff Steiner');
$mail->addAddress('rocio.suarez@atd-quartmonde.org','Rocio Suarez');
$mail->addAddress('expressionspubliques@atd-quartmonde.org','PEPS');
$mail->addAddress('jean.tonglet@atd-quartmonde.org','Jean Tonglet');
$mail->addAddress('m.herbignat13@orange.fr','Martine Herbignat');
$mail->addAddress('editions@atd-quartmonde.org','Editions');
$mail->addAddress('secretariat.dg@atd-quartmonde.org','Secrétariat DG');
$mail->addAddress('communication.emploi@atd-quartmonde.org','Loïc Hamon');
$mail->addReplyTo('webmestre@atd-quartmonde.org', 'Webmestre');
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');

//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = "Statistiques des sites pour le $yesterday_nice";

$mail->Body    = $message;
//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

$mail->setLanguage('fr');

$mail->CharSet = "UTF-8";

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}
