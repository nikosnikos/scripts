# C'est quoi tout ça ?
Le répertoire contient les scripts pour mirrorer des sites web qui sont chez Gandi.

# Comment ça marche ?
Le script `mirror_gandi_sites.sh` est lancé par cron dans la crontab du user root tous les jours pour recopier les fichiers et la base de données d'un site web chez Gandi. Un accès ssh doit être configuré dans .ssh/config pour que ça marche.
On peut bien évidemment lancer le script à la main également.

Le script `update_mirror_sites.sh` met à jour la base de donnée d'un des sites mirroré. En effet lors du mirroring et pour ne pas surcharger le serveur la base de donnée n'est pas mise à jour. Pour avoir un site à jour il faut donc lancer ce script.

# T'as des exemples ?
Voici des exemples d'utilisation :

En cron :

```
0 03 * * * /root/scripts/mirror/mirror_gandi_sites.sh atd-quartmonde.fr 510292 www.atd-quartmonde.fr site_web_france

00 04 * * * /root/scripts/mirror/mirror_gandi_sites.sh atd-quartmonde.org 1729316 atd-quartmonde.org atdqm
```

Et donc en ligne :

```
/root/scripts/mirror/mirror_gandi_sites.sh atd-quartmonde.fr 510292 www.atd-quartmonde.fr site_web_france

/root/scripts/mirror/update_gandi_sites.sh atd-quartmonde.fr
```
