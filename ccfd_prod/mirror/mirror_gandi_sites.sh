#!/bin/sh

# Script de backup de la base de données récupéré ici : http://groups.gandi.net/fr/topic/gandi.fr.hebergement.simple/32364

PATH=/usr/sbin/:/usr/bin/:/sbin/:/bin/
export PATH
curr_date=`date +"%Y-%m-%d %H:%M"`
email="webmestre@atd-quartmonde.org" #,sarrotjc@free.fr,gabrielle.serra@wasi.fr,informatique.france@atd-quartmonde.org" #email des alertes
gandi_url=sftp.dc0.gpaas.net
here_site_id=$1
gandi_user=$2
gandi_site_id=$3
gandi_mysql_db=$4

base_path="/var/www/$here_site_id/htdocs/"
base_ls=$(ls -l /var/www/$here_site_id/ | grep htdoc)
base_owner=$(echo "$base_ls" | awk '{print $3}')
base_group=$(echo "$base_ls" | awk '{print $4}')

dump_file=$gandi_mysql_db.dump.sql

#set -o noclobber
#set -o pipefail
#ulimit -f 20480
renice 10 "$$" > /dev/null

#output=$(
#  exec 2>&1

  echo ""
  
  echo "Mirror du site $here_site_id de l'instance $gandi_user"
  date '+%F %T %z'
  echo ""
  
  uptime
  echo ""
  
  df -h . 2> /dev/null
  echo ""

#  set -x

  cd $base_path || exit

  echo "Recupération du site"
  lftp -u $gandi_user, -e "set xfer:log true; mirror --allow-chown --parallel=50 --delete /lamp0/web/vhosts/$gandi_site_id/htdocs/. $base_path; quit" sftp://$gandi_url

  echo "Met les bonnes permissions sur les fichiers"
  chown -R $base_owner:$base_group .
  # chmod tous les dossiers qui ne sont pas déjà en 750
  find . -type d ! -perm 750 -exec chmod 750 {} \;
  # chmod tous les fichiers qui ne sont pas déjà en 640
  find . -type f ! -perm 640 -exec chmod 640 {} \;
  chmod 400 ./wp-content/mu-plugins/*
  chmod 400 ./wp-config.php

  echo "Récupération de la dernière sauvegarde de la base"
  lftp -u $gandi_user, -e "set xfer:log true; set xfer:clobber true; get /lamp0/web/includes/backup/daily/db-backup-0/$dump_file.gz; quit" sftp://$gandi_url

  echo "import de la base à jour"
  gunzip $dump_file.gz
  #db_pass=$(grep DB_PASSWORD wp-config.php |sed 's/^.*DB_PASSWORD'"'"'\s*,\s*'"'"'\([^'"'"']*\).*/\1/')
  #mysql -h localhost -u $gandi_mysql_db -p$db_pass  < $dump_file
  #rm $dump_file

  echo "OK. Import terminé. Site $here_site_id dupliqué."
  echo "Sript de mirror executé avec les paramètres suivant : $*" | mail -s "Execution du script de mirroring" webmestre@atd-quartmonde.org

#) && echo "$output" | mail -s "[$here_site_id] DAILY MIRROR LOGS" "$email"

#echo "$output"
#echo "$output" >> /var/www/$here_site_id/logs/mirror.log
