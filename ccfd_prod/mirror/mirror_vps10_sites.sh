#!/bin/sh

# Script de backup de la base de données récupéré ici : http://groups.gandi.net/fr/topic/gandi.fr.hebergement.simple/32364

PATH=/usr/sbin/:/usr/bin/:/sbin/:/bin/
export PATH
curr_date=`date +"%Y-%m-%d %H:%M"`
email="webmestre@atd-quartmonde.org" #,sarrotjc@free.fr,gabrielle.serra@wasi.fr,informatique.france@atd-quartmonde.org" #email des alertes
vps10_url=vps293210.ovh.net
here_site_id=$1
vps10_user=$2
vps10_site_id=$3
vps10_mysql_db=$4

base_path="/var/www/$here_site_id/htdocs/"
base_ls=$(ls -l /var/www/$here_site_id/ | grep htdoc)
base_owner=$(echo "$base_ls" | awk '{print $3}')
base_group=$(echo "$base_ls" | awk '{print $4}')

dump_file=$vps10_mysql_db.sql

#set -o noclobber
#set -o pipefail
#ulimit -f 20480
renice 10 "$$" > /dev/null

#output=$(
#  exec 2>&1

  echo ""
  
  echo "Mirror du site $here_site_id de l'instance $vps10_user"
  date '+%F %T %z'
  echo ""
  
  uptime
  echo ""
  
  df -h . 2> /dev/null
  echo ""

#  set -x

  cd $base_path || exit

  echo "Recupération du site"
  lftp -u $vps10_user, -e "set xfer:log true; mirror --allow-chown --parallel=50 --delete /htdocs/. $base_path; quit" sftp://$vps10_url -p 22168

  echo "Met les bonnes permissions sur les fichiers"
  chown -R $base_owner:$base_group .
  # chmod tous les dossiers qui ne sont pas déjà en 750
  find . -type d ! -perm 750 -exec chmod 750 {} \;
  # chmod tous les fichiers qui ne sont pas déjà en 640
  find . -type f ! -perm 640 -exec chmod 640 {} \;
  #chmod 400 ./wp-content/mu-plugins/*
  #chmod 400 ./wp-config.php

  echo "Récupération de la dernière sauvegarde de la base"
  lftp -u $vps10_user, -e "set xfer:log true; set xfer:clobber true; get /$dump_file; quit" sftp://$vps10_url -p 22168

  echo "import de la base à jour"
  db_pass=$(grep "^\$db_url" sites/default/settings.php | sed 's#\$db_url\s*=\s*'"'"'mysql\://[^\:]*\:\([^@]*\)@.*$#\1#')
  mysql -h localhost -u $vps10_mysql_db -p$db_pass  < $dump_file
  rm $dump_file

  echo "OK. Import terminé. Site $here_site_id dupliqué."
  echo "Sript de mirror executé avec les paramètres suivant : $*" | mail -s "Execution du script de mirroring" webmestre@atd-quartmonde.org

#) && echo "$output" | mail -s "[$here_site_id] DAILY MIRROR LOGS" "$email"

#echo "$output"
#echo "$output" >> /var/www/$here_site_id/logs/mirror.log
