#!/bin/bash

sitepath=/var/www/$1/htdoc/

cd $sitepath

db_pass=$(grep DB_PASSWORD wp-config.php |sed 's/^.*DB_PASSWORD'"'"'\s*,\s*'"'"'\([^'"'"']*\).*/\1/')
db_name=$(grep DB_NAME wp-config.php |sed 's/^.*DB_NAME'"'"'\s*,\s*'"'"'\([^'"'"']*\).*/\1/')
db_user=$(grep DB_USER wp-config.php |sed 's/^.*DB_USER'"'"'\s*,\s*'"'"'\([^'"'"']*\).*/\1/')

dump_file=$db_name.dump.sql

mysql -h localhost -u $db_user -p$db_pass $db_name < $dump_file

