#!/bin/bash

emAdy="nicolas.duclos@atd-quartmonde.org;support@atd-quartmonde.org"
emSub="Warning: Nouveaux fichiers à 0 dans nextcloud"

# Fichier avec les fichiers à zéro trouvés par ce script, on met la date pour info.
date > new_check.log

# Parcours les fichiers de tous les utilisateurs
for fs in /var/www/data/*/files; do

    while read -d '' -r; do
        # Enregistre le fichier à zéro dans la liste de ceux trouvé par ceux script
        echo -e "$REPLY\n\n" >> new_check.log
        # Regarde s'il n'avait pas déjà été trouvé lors du dernier script
        grep "$REPLY" last_check.log
        # Si il n'avait pas été trouvé, le met dans la liste des nouveau fichiers à zéro
        if [[ $? != 0 ]]; then
            # Ne prend pas le dossier perso de Vincent qui a des caractères trop chelous
            echo "$REPLY"|grep -vq "vincent.lucy/files/Documents/perso" && echo "on prend pas"
            if [[ $? == 0 ]]; then
                arr+=( "$REPLY\n\n" )
            fi
        fi
    done < <(find $fs -size 0 -print0)

done

# Les fichiers à zéro de ce scripts sont enregistrés dans last_check.log
cat new_check.log > last_check.log

# S'il y a des nouveaux fichiers à zéro on envoie un mail.
if [[ ! -z ${arr[@]} ]]; then

    echo -e "Voici la liste des nouveaux fichiers vides :\n\n ${arr[@]}" | mail -s "$emSub" "$emAdy"

fi
