#!/bin/bash

########################################################################
#
# Le script check la taille des dossiers du user partage :
#
# - Si le quota d'un dossier est bientôt dépassé (>90%) un mail est
#   envoyé à $alert_mailto qui devra avertir les personnes concernées ;
#
# - si le quota est dépassé le script met le dossier en lecture seule,
#   ATTENTION : si le ménage est fait sur le dossier, le script ne remet
#   pas le dossier en écriture, il faudra le faire manuellement ;
#
# - si aucun problème de quota, un mail est envoyé à $info_mailto pour
#   signaler que le script a bien tourné.
#
# Le quota des dossiers est par défaut $quota (en octets), pour définir
# une limite plus haute pour certains dossiers remplir $other_quotas.
#
# Améliorations possibles :
# - Vérifier (mieux) que tout se passe bien au niveau de curl
# - Envoyer directement le mail depuis le script
#
########################################################################

#
# Les constantes
#

# destinataires du mail d'alerte en cas de problème de quota
alert_mailto="nicolas.duclos@atd-quartmonde.org;informatique.test@atd-quartmonde.org"
#destinataires du mail pour dire juste que le script a tourné
info_mailto="nicolas.duclos@atd-quartmonde.org"
# Quota par défaut : 20GB
default_quota=20000000
# Quotas spécifiques par dossier dans la variable other_quotas
read -r -d '' other_quotas << EOM
'Pauvreté-DimensionsMesures':35000000
'communication':30000000
EOM

# Base de nextcloud
nextcloud_folder="/var/www"
# Base des dossiers du user partage
partage_user_folder="$nextcloud_folder/data/partage/files/"
# L'url de nextcloud pour faire du curl
nextcloud_url=$(cat "$nextcloud_folder/config/config.php"|grep 'overwrite.cli.url' |sed "s/^.*=>\s*'\([^']\+\)'\s*,/\1/")
# Le mdp de partage (différent en fonction de l'env)
partage_pwd="nid*opFag4Osda"

script_folder=$(pwd)

#
# Vérifications qu'il y a tout ce qu'il faut
#

command -v curl >/dev/null 2>&1 || { echo >&2 "Curl n'est pas installé. Arrêt du script."; echo "Curl n'est pas installé. Arrêt du script." |mail -s "Erreur: Script check des quotas" "$alert_mailto"; exit 1; }
command -v xml2 >/dev/null 2>&1 || { echo >&2 "Xml2 n'est pas installé. Arrêt du script."; echo "Xml2 n'est pas installé. Arrêt du script." |mail -s "Erreur: Script check des quotas" "$alert_mailto"; exit 1; }

#
# Fonctions
#

# TODO Pas réussi à faire fonctionner cette fonction call_ocs correctement !!
# call_ocs "-G -sS" "shares" '--data-urlencode "path=/InformatiqueDuMouvement"' "/ocs/meta/statuscode" "'s/^.*[^0-9]\([0-9]\+\)$/\1/g'"

# TODO supprimer ou faire foncitonner correctement !
# Call nextcloud OCS service, check que ça a bien fonctionné
# Voir https://docs.nextcloud.com/server/13/developer_manual/core/ocs-share-api.html et https://docs.nextcloud.com/server/13/developer_manual/client_apis/OCS/index.html
# $1 : Les options curl à utiliser, typiquement "-G -sS" ou "-X PUT" ou ...
# $2 : Le service OCS à appeler, trois possiblités : "shareid" ou "shares" ou "users"
# $3 : Le chemin par rapport au service de base et les paramètres http.
#      Exemple : "/" ou " --data-urlencode "path=$group_folder" --data-urlencode "reshares=true"" ou "/10?permissions=32"
# $4 : L'élément du xml résultat du call qu'on veut répucpérer.
#      Exemple : "/ocs/data/element/sharewith"
function call_ocs {
  ocs_options="$1"
  if [[ "$2" = "shares" ]]; then
    ocs_service="ocs/v2.php/apps/files_sharing/api/v1/shares $3"
  elif [[ "$2" = "shareid" ]]; then
    ocs_service="ocs/v2.php/apps/files_sharing/api/v1/shares/$3"
  elif [[ "$2" = "users" ]]; then
    ocs_service="ocs/v1.php/cloud/users/$3"
  else
    echo "Mauvais call de la fonction call_ocs du script"
    exit 1
  fi
  ocs_element="$4"
  ocs_sed_data="$5"

  # Retour de l'appel curl
  ocs_return=$(curl $ocs_options -H "OCS-APIRequest: true" -u partage:$partage_pwd $nextcloud_url/$ocs_service)
  echo -e "$ocs_return"

  ocs_status=$(echo -e "$ocs_return" |xml2 |grep "/ocs/meta/statuscode=" | sed 's/^.*[^0-9]\([0-9]\+\)$/\1/g' 2>&1)
  if [[ "$ocs_status" -ne "200" ]]; then
      echo -e "Attention, erreur lors du call du service OCS, erreur $ocs_status :\n\ncurl $ocs_options -H \"OCS-APIRequest: true\" -u partage:XXXX $nextcloud_url/$ocs_service\n\n$ocs_return"|mail -a "Content-Type: text/plain; charset=UTF-8" -s "[partage] Erreur dans le script de check des quotas" "$alert_mailto"
      return 0
  fi

  ocs_data==$(echo -e "$ocs_return" |xml2 |grep "$ocs_element=" | sed $ocs_sed_data 2>&1)
  echo $ocs_data
  return $ocs_data
}

# Print les emails des users avec qui est partagé le dossier et aussi des
# membres des éventuels groupes avec qui le dossier est partagé.
# $1 : Le nom du dossier pour lequel on regarde les partages.
function print_emails {
  partage_folder="$1"
  # Récupération des emails de tout le monde
  ocs_return=$(curl -G -sS -H "OCS-APIRequest: true" -u partage:$partage_pwd $nextcloud_url/ocs/v2.php/apps/files_sharing/api/v1/shares --data-urlencode "path=$partage_folder" --data-urlencode "reshares=true")

  ocs_status=$(echo -e "$ocs_return" |xml2 |grep "/ocs/meta/statuscode=" | sed 's/^.*[^0-9]\([0-9]\+\)$/\1/g' 2>&1)
  if [[ "$ocs_status" -ne "200" ]]; then
      echo -e "Attention, erreur lors du call du service OCS, erreur $ocs_status :\n\n$ocs_return"|mail -a "Content-Type: text/plain; charset=UTF-8" -s "[partage] Erreur dans le script de check des quotas" "$alert_mailto"
  else
    # pour que le for marche s'il y a des espaces dans le sahreid (cas des groupes)
    OLDIFS=$IFS
    IFS=$(echo -en "\n\b")
    # Parcours des share ids
    for sharewith in $(echo -e "$ocs_return" |xml2 |grep "/ocs/data/element/share_with=" | sed 's/^[^=]\+=\(.*\)$/\1/g'); do
      # On encode pour être sûr que ça passe avec curl (cas avec espace, accents, ...)
      sharewith=$(php -r "echo rawurlencode('$sharewith');")
      # récup du mail si c'est un user avec mail
      shared_user_email=$(curl -X GET -H "OCS-APIRequest: true" -u partage:$partage_pwd $nextcloud_url/ocs/v1.php/cloud/users/$sharewith|xml2 |grep "/ocs/data/email=" | sed 's/^[^=]\+=\(.*\)$/\1/g' )
      # Si on ne trouve pas de mail on regarde si c'est un groupe
      if [[ "$shared_user_email" = "" ]]; then
        for sharewithgroup in $(curl -X GET -H "OCS-APIRequest: true" -u partage:$partage_pwd $nextcloud_url/ocs/v1.php/cloud/groups/$sharewith|xml2| grep "/ocs/data/users/element=" | sed 's/^[^=]\+=\(.*\)$/\1/g' ); do
          # Pour chaque user du group, on récupère le mail
          group_email=$(curl -X GET -H "OCS-APIRequest: true" -u partage:$partage_pwd $nextcloud_url/ocs/v1.php/cloud/users/$sharewithgroup|xml2 |grep "/ocs/data/email=" | sed 's/^[^=]\+=\(.*\)$/\1/g' )
          shared_user_email="$shared_user_email $group_email"
        done
      fi
      # Si on n'a toujours pas d'email, on donne le user ou le groupe
      if [[ "$shared_user_email" = "" ]]; then
        shared_user_email="L'adresse email de $sharewith"
      fi
      emails_list+=( "$shared_user_email" )
    done
    IFS=$OLDIFS
  fi
  echo "${emails_list[@]}"
}

#
# Go le script
#

# On va dans le dossier du user partage
cd $partage_user_folder
# 0 si pas de problème de quota, 1 sinon
found=0

# Pour chaque fichier/dossier du user partage
for partage_folder in *; do
    # Taille du dossier en octets
    size=$(du -s "$partage_folder"| sed "s/^\([0-9]\+\).*$/\1/")
    # Taille du dossier lisible ex : 4,5G (ou 4.5G sur un serveur pas fr_FR)
    size_human=$(du -sh "$partage_folder"| sed "s/^\([0-9\.,]\+[A-Z]\).*$/\1/")
    # Quota du dossier
    folder_quota=$(echo -e "$other_quotas" | grep "'$partage_folder'" |sed "s/^'$partage_folder':\([0-9]\+\)\s*$/\1/g")
    if [ -z "$folder_quota" ]; then
        folder_quota=$default_quota
    fi
    folder_quota_human=$(($folder_quota/1000000))
    # 90% du quota
    near_quota=$(echo "0.9*$folder_quota" | bc)

    # TODO : Tester avec un dossier avec espaces
    # L'éventuel fichier avec les permissions des différents partages s'il y a
    # un blocage en cours sur ce dossier
    shares_perms="$script_folder/$partage_folder.shares_perms.conf"

    # Si $partage_folder.shares_perms.conf existe c'est donc que le dossier est
    # bloqué mais en même temps le quota est repassé en dessous de la barre.
    # => On débloque le dossier et on prévient.
    if [[ -f $shares_perms && "$size" -lt "$folder_quota" ]]; then
        # On remet les permissions sur les partages comme avant blocage
        while read line; do
            shareid=$(echo "$line" | sed 's/^\([0-9]\+\)\=.*$/\1/g')
            permission=$(echo "$line" | sed 's/^[0-9]\+\=\([0-9]\+\)$/\1/g')
            curl_return=$(curl -X PUT -H "OCS-APIRequest: true" -u partage:$partage_pwd $nextcloud_url/ocs/v2.php/apps/files_sharing/api/v1/shares/$shareid?permissions=$permission)
            curl_status=$(echo -e "$curl_return" |xml2 |grep "/ocs/meta/statuscode=" | sed 's/^.*[^0-9]\([0-9]\+\)$/\1/g')
            if [[ "$curl_status" -ne "200" ]]; then
                echo -e "Attention, le déblocage du dossier $partage_folder ne c'est pas bien passé :\n\n$curl_return"|mail -a "Content-Type: text/plain; charset=UTF-8" -s "Erreur dans le déblocage de $partage_folder" "$alert_mailto"
            fi
        done <"$shares_perms"
        sp_ext=$(date +%Y%m%d)
        # On le sauvegarde au cas où
        mv $shares_perms $shares_perms.$sp_ext
        # Et on envoie un mail pour prévenir du déblocage
        folder_emails=$(print_emails "$partage_folder")
        echo -e "Le dossier \"$partage_folder\" du user partage est maintenant à $size_human, c'est à dire à nouveau en dessous de son quota : ${folder_quota_human}GB et à nouveau accessible en écriture pour ses utilisateurs-trices.\n\nAction : Envoyer un mail à ses utilisateurs-trices pour les avertir.\n\nMail à envoyer :\n-------------------------------------\nÀ: $folder_emails\n-------------------------------------\nSujet: [partage] Dossier $partage_folder débloqué sur partage.\n-------------------------------------\nBonjour,\n\nSur l'outil partage d'ATD (https://partage.atd-quartmonde.org), le répertoire \"$partage_folder\" de votre groupe est maintenant en dessous de ${folder_quota_human}GB. Sa taille actuelle est de $size_human. Il vient donc d'être débloqué.\n\nN'hésitez pas à nous contacter pour toute question.\n\nAmicalement\nL'équipe informatique\n-------------------------------------" |mail -a "Content-Type: text/plain; charset=UTF-8" -s "Info sur le quota du dossier $partage_folder de partage" "$alert_mailto"
        found=1

    # Si on est à 90% du quota (transformé en entier) => On averti
    elif [[ "$size" -gt "${near_quota%%.*}" && "$size" -lt "$folder_quota" ]]; then
        folder_emails=$(print_emails "$partage_folder")
        echo -e "Un dossier du user partage à 90% du quota :\n\n\"$partage_folder\" : $size_human\n\nAction : Envoyer un mail à ses utilisateurs-trices pour les avertir, si le quota est dépassé le script $0 met le dossier en lecture seule.\n\nMail à envoyer :\n-------------------------------------\nÀ: $folder_emails\n-------------------------------------\nSujet: [partage] Attention quota du dossier $partage_folder bientôt dépassé\n-------------------------------------\nBonjour,\n\nSur l'outil partage d'ATD (https://partage.atd-quartmonde.org), le répertoire \"$partage_folder\" de votre groupe a atteint la taille de $size_human. La taille des dossiers partagés est limité à ${folder_quota_human}GB pour préserver l'espace disque et réduire le coût du serveur.\n\nAttention : Le dossier sera bloqué automatiquement en écriture dès qu'il atteindra la taille de ${folder_quota_human}GB.\n\nN'hésitez pas à nous contacter pour toute question.\n\nAmicalement\nL'équipe informatique\n-------------------------------------" |mail -a "Content-Type: text/plain; charset=UTF-8" -s "Alerte sur le quota du dossier $partage_folder de partage" "$alert_mailto"
        found=1

    # Si on a dépassé le quota et qu'un blocage est en cours
  elif [[ -f $shares_perms && "$size" -gt "$folder_quota" ]]; then
        # Envoie d'un mail de rappel
        folder_emails=$(print_emails "$partage_folder")
        echo -e "Le dossier \"$partage_folder\" est toujours bloqué, sa taille est de : $size_human, son quota ${folder_quota_human}GB.\n\nAction : Envoyer un mail de relance à ses utilisateurs-trices ? On a ensuite deux options : Soit ils-elles font le ménage, soit on augmente le quota dans $0. Et une fois que le ménage est fait ou le quota augmenté : soit ils-elles attendent 24h que le cron lance le script qui va débloquer le dossier, soit on lance le script $0 directement depuis le serveur pour un déblocage immediat.\n\nMail à envoyer :\n-------------------------------------\nÀ: $folder_emails\n-------------------------------------\nSujet: [partage] Dossier $partage_folder toujours bloqué !\n-------------------------------------\nBonjour,\n\nLe répertoire \"$partage_folder\" sur l'espace partage d'ATD (https://partage.atd-quartmonde.org) est toujours bloqué. En effet sa taille est de $size_human alors qu'il ne doit pas dépasser la taille de ${folder_quota_human}GB. Pour rappel : il est possible de supprimer des fichiers dedans pour faire le ménage et revenir en dessous du quota.\n\nUne fois le ménage fait dans le dossier, celui-ci sera débloqué dans les 24h. Si vous souhaitez un débloquage plus rapide, contactez-nous.\n\nAmicalement\nL'équipe informatique\n-------------------------------------" |mail -a "Content-Type: text/plain; charset=UTF-8" -s "Dossier $partage_folder toujours bloqué dans partage." "$alert_mailto"
        found=1

    # Si on a dépassé le quota et qu'il n'y a pas de blocage en cours => il faut bloquer.
    elif [[ "$size" -gt "$folder_quota" ]]; then
        folder_emails=$(print_emails "$partage_folder")
        echo -e "Un dossier du groupe partage a dépassé son quota de ${folder_quota_human}GB:\n\n\"$partage_folder\" : $size_human\n\nAction : Envoyer un mail à ses utilisateurs-trices pour les en avertir. On a ensuite deux options : Soit ils-elles font le ménage, soit on augmente le quota dans $0. Et une fois que le ménage est fait ou le quota augmenté : soit ils-elles attendent 24h que le cron lance le script qui va débloquer le dossier, soit on lance le script $0 directement depuis le serveur pour un déblocage immediat.\n\nMail à envoyer :\n-------------------------------------\nÀ: $folder_emails\n-------------------------------------\nSujet: [partage] Important : Dossier $partage_folder bloqué !\n-------------------------------------\nBonjour,\n\nLe répertoire $partage_folder sur l'espace partage d'ATD (https://partage.atd-quartmonde.org) est maintenant bloqué, il a en effet atteint la taille de $size_human, il est toutefois encore possible de supprimer des fichiers dedans pour faire le ménage et revenir en dessous du quota de ${folder_quota_human}GB.\n\nUne fois le ménage fait dans le dossier, celui-ci sera débloqué dans les 24h. Si vous souhaitez un débloquage plus rapide, contactez-nous.\n\nAmicalement\nL'équipe informatique\n-------------------------------------" |mail -a "Content-Type: text/plain; charset=UTF-8" -a "X-Priority: 1 (Highest)" -a "Importance: high" -s "Attention ! Quota dépassé sur $partage_folder dans partage" "$alert_mailto"
        found=1

        # Pour le dossier du groupe et ses sous dossiers
        while read -d '' -r; do
            group_folder=$REPLY
            # Limite le partage à la suppression des fichiers/dossier, plus de modifs possibles
            # Voir https://docs.nextcloud.com/server/13/developer_manual/core/ocs-share-api.html et https://docs.nextcloud.com/server/13/developer_manual/client_apis/OCS/index.html
            # Liste des partages du dossier
            for shareid in $(curl -G -sS -H "OCS-APIRequest: true" -u partage:$partage_pwd $nextcloud_url/ocs/v2.php/apps/files_sharing/api/v1/shares --data-urlencode "path=$group_folder" --data-urlencode "reshares=true" |xml2 |grep "/ocs/data/element/id=" | sed 's/^.*[^0-9]\([0-9]\+\)$/\1/g'); do
                # Récupération du droit actuel
                sharepermission=$(curl -G -sS -H "OCS-APIRequest: true" -u partage:$partage_pwd $nextcloud_url/ocs/v2.php/apps/files_sharing/api/v1/shares/$shareid |xml2 |grep "/ocs/data/element/permissions=" |sed 's/^.*[^0-9]\([0-9]\+\)$/\1/g')
                # Enregistrement de l'ancien droit pour pouvoir le remettre plus tard
                echo "$shareid=$sharepermission">>$shares_perms
                # Modification de chaque partage par les ids trouvés dans le requête précédente
                # permissions - (int) 1 = read; 2 = update; 4 = create; 8 = delete; 16 = share; 31 = all (default: 31, for public shares: 1)
                # permission = 9 => read + delete
                curl_return=$(curl -X PUT -H "OCS-APIRequest: true" -u partage:$partage_pwd $nextcloud_url/ocs/v2.php/apps/files_sharing/api/v1/shares/$shareid?permissions=9)
                curl_status=$(echo -e "$curl_return" |xml2 |grep "/ocs/meta/statuscode=" | sed 's/^.*[^0-9]\([0-9]\+\)$/\1/g')
                if [[ "$curl_status" -ne "200" ]]; then
                    echo -e "Attention, le blocage du dossier $group_folder ne c'est pas bien passé. Le bloquer à la main ?\nRetour :\n\n$curl_return"|mail -a "Content-Type: text/plain; charset=UTF-8" -s "Erreur dans le blocage de $group_folder" "$alert_mailto"
                fi
            done
        done < <(find "$partage_folder" -type d -print0)

    fi
done

# Pas de problème de quota
if [[ $found == 0 ]]; then
    echo "tout est OK" |mail -s "Info: Quota des dossiers partage OK" "$info_mailto"
fi
