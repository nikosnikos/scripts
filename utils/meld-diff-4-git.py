#!/usr/bin/python

# Permet de faire un diff git avec meld sans que meld sorte une erreur
# Source : https://github.com/zhanglongqi/linux-tips/blob/master/git/git_merge_tool.md
import sys
import os

os.system('meld "%s" "%s"' % (sys.argv[2], sys.argv[5]))
