#!/usr/bin/env python
# -*- coding: utf-8 -*-

from optparse import OptionParser
import sys

import MySQLdb

reload(sys);
sys.setdefaultencoding("utf8")

def main():
    parser = OptionParser(usage="""

%prog -h (pour avoir l'aide)
%prog -l n [options]
%prog -I liste,d'adresses,mail [options]
%prog -c

Crée un lot d'adresses à exporter au fichier csv

Exemple : %prog -l 20 -M 10 -m 5 -i toto@atd,titi@atd -f

=> Crée un fichier csv avec un lot d'adresses mails dont la taille total ne dépasse pas 20G avec la plus grosse boite autour de 10G et le reste des boites en dessous de 5G incluant les adresses toto@atd et titi@atd avec des boites qui n'ont pas de filtres.
""")

    parser.add_option('-l', '--limit',
                      type='int',
                      help="Le nombre total de boites importées.",
                      default=40)

    parser.add_option('-M', '--bigmax',
                      type='int',
                      help='La taille approximative de la boite mail la plus grosse à importer, en Go. Si non précisé, prend la boite la plus grosse.',
                      default=50)

    parser.add_option('-N', '--nb_bigmax',
                      type='int',
                      help='Le nombre de grosses boites à importer. Defaut : 1',
                      default=1)
  
    parser.add_option('-m', '--smallmax',
                      type='int',
                      help='La taille max des boites mails à importer (en dehors de la grosse boite), en Go. Defaut : 8',
                      default=8)

    parser.add_option('-i', '--include',
                      type='string',
                      help='Les logins (séparées par une virgule) à inclure dans le lot. Dans la limite de -l.')

    parser.add_option('-I', '--include_only',
                      type='string',
                      help="Les logins (séparées par une virgule). Seules les adresses des logins précisées ici seront dans le lot. Aucune autre. Dans la limite de 60Go.")

    parser.add_option('-f', '--filter',
                      action='store_true',
                      default=False,
                      help="Si -f le script ne s'occupe pas de savoir s'il y a des filtres ou pas. Sinon par défaut il ne prend que les adresses qui n'ont pas de filtre.")

    parser.add_option('-a', '--alias',
                      action='store_true',
                      default=False,
                      help="Si -a on prend aussi les mails avec des alias. Sinon ne prend que les mails qui n'ont pas d'alias.")

    parser.add_option('-d', '--debug',
                      action='store_true',
                      default=False,
                      help="Si -d, tout se passe comme pour générer un lot mais la base n'est pas mise à jour et les requêtes sont printées.")

    parser.add_option('-t', '--test',
                      action='store_true',
                      default=False,
                      help="Si -t, tout se passe comme pour générer un lot mais la base n'est pas mise à jour.")

    parser.add_option('-c', '--cancel',
                      action='store_true',
                      default=False,
                      help="Si -c le script annule le dernier update.")

    opts, args = parser.parse_args()

    if len(args) != 0:
        parser.error("Il ne doit pas y avoir d'argument dans ce script. ./creatLot.py -h pour avoir de l'aide.")
        sys.exit(1)

    limit = opts.limit

    include_only = opts.include_only
    if include_only:
        include_only = include_only.split(',')

    include = opts.include
    if include:
        include = include.split(',')

    test = opts.test or opts.debug

    if (opts.cancel and len(sys.argv) > 2):
      parser.error("Il ne doit pas y avoir d'autres options avec l'option -c.")
      sys.exit(1)

    try:
        db = MySQLdb.connect(host="localhost", # your host, usually localhost
                       user="extranet", # your username
                        passwd="47Bakitr!", # your password
                        db="extranet_help") # name of the data base

        # you must create a Cursor object. It will let
        #  you execute all the query you need
        cur = db.cursor() 

        # Numéro du lot à générer (= le numéro du dernier + 1)
        sql = "SELECT MAX(migrated) FROM migration_mail"
        cur.execute(sql)
        row = cur.fetchone()
        oldnumlot = int(row[0])        

        if opts.cancel:
          update_cancel = "UPDATE migration_mail SET migrated = 0 WHERE migrated = {0}".format(oldnumlot)

          print ""
          print "Annulation du dernier lot, le {0} ....".format(oldnumlot)
          print ""

          cur.execute(update_cancel)
          exit()

        print '"email";"prenom";"nom";"login_arsys";"password_arsys";"serveur_imap_arsys";"password_BlueMind";"json_alias";"mail_par_defaut"'

        # Nouveau numéro de lot
        numlot = oldnumlot + 1

        # La taille des boites à importer
        actualsize = 0

        # La requête de base à compléter en fonction de chaque cas
        select_base = """SELECT m1.email, m1.firstname, m1.lastname, m1.arsys_key, m1.password, m1.sizeMB, 
GROUP_CONCAT(m2.alias SEPARATOR '":true,"') AS aliases, 
m1.login
FROM migration_mail m1
LEFT OUTER JOIN migration_mail_aliases m2 ON m1.email = m2.email
WHERE (m1.observation_2 NOT LIKE 'suppr%' OR m1.observation_2 IS NULL OR m1.observation_2 = '') 
AND m1.migrated = 0"""

        if not opts.alias:
          select_base += """
AND m2.alias IS NULL
AND m1.email NOT LIKE '%@atd-fourthworld.org'
AND m1.email NOT LIKE '%@atd-cuartomundo.org'
AND m1.email NOT LIKE '%.be'"""

        if not opts.filter:
          select_base += """
AND (filter = 0 OR filter = -1)"""

        select_base += """
{0}
GROUP BY m1.email, m1.firstname, m1.lastname, m1.arsys_key, m1.password, m1.sizeMB, m1.login
{1}"""

        # Si un include est demandé on commence par ça pour être sûr d'avoir toutes les boites concernées
        to_include = None
        if include_only:
          to_include = include_only
        elif include:
          to_include = include

        if to_include:
          to_add = ' AND m1.login in ("'
          index = 0
          for inc in to_include:
            if index > 0:
              to_add += '","'
            to_add += inc
            index += 1
          to_add += '")'

          select_include = select_base.format(to_add,' LIMIT {0}'.format(limit))

          if opts.debug:
            print "# Requête pour récupérer les boites de l'option -i ou -I"
            print select_include

          cur.execute(select_include)

          actualsize = printlines(cur,numlot,actualsize,test)
        # fin du if include

        # Si include_only on s'arrête là on importe pas d'autres boites.
        if include_only:
          if opts.debug:
            print "# Il y a l'option -I => on n'intègre aucune autre boite au lot"
          if test:
            print 'taille totale : {0}'.format(actualsize)
          cur.close()
          exit()

        # Récupération des boites opts.nb_bigmax boites les plus grosse (par défaut c'est juste la plus grosse)
        select_max = select_base.format(' AND m1.sizeMB < {0}'.format(opts.bigmax*1000),' ORDER BY m1.sizeMB DESC LIMIT {0}'.format(opts.nb_bigmax))

        if opts.debug:
          print "# Requête pour récupérer les boites les plus grosses (=> {0} boite(s) autour de {1} Go)".format(opts.nb_bigmax,opts.bigmax)
          print select_max

        cur.execute(select_max)

        actualsize = printlines(cur,numlot,actualsize,test)

        # Récupération du reste des boites jusqu'à ce qu'on ait atteint la taille "limit"
        and_where = " AND sizeMB < {0}".format(opts.smallmax * 1000)

        select_rest = select_base.format(and_where,"ORDER BY RAND() LIMIT {0}".format(limit - opts.nb_bigmax))
        if opts.debug:
          print "# Requête pour sélectionner le reste des boites jusqu'à ce qu'on arrive à la limite de l'option -l"
          print select_rest
        cur.execute(select_rest)

        actualsize = printlines(cur,numlot,actualsize,test)

        if test:
          print 'taille totale : {0}'.format(actualsize)

        cur.close() 

    except Exception, e:
        print e

def printlines(cursor,numlot,actualsize,test):

    # parcourt all the cells of all the rows
    for row in cursor.fetchall() :
      actualsize += row[5]

      arsysemail = row[0]
      firstname = row[1]
      lastname = row[2]
      arsyskey = row[3]
      password = row[4]
      aliases = row[6]
      login = row[7]

      teclibemail = login + '@atd-quartmonde.org'

      if not aliases:
        aliases = ''
      else:
        aliases = '"' + aliases + '":true'

      import re
      aliases = re.sub('"' + teclibemail + '":true,?', '', aliases)

      if teclibemail != arsysemail:
        if aliases:
          aliases += ','
        aliases += '"' + arsysemail + '":true'

      aliases = '{' + aliases + '}'

      # on doit respecter le format bizarroïde de Benjamin dans les alias : {"informatique.france@atd-quartmonde.org": true,"test@atd-quartmonde.org": true} et pas de guillemets protecteurs
      #print "{0};{1};{2};{3};{4};{5};{6};{7};{8}".format(teclibemail, '"'+firstname+'"', '"'+lastname+'"', arsyskey, password, '217.76.129.93', password, '{"' + aliases + '"}', arsysemail)
      print "{0};{1};{2};{3};{4};{5};{6};{7};{8}".format(teclibemail, '"'+firstname+'"', '"'+lastname+'"', arsyskey, password, '217.76.129.93', password, aliases, arsysemail)
      
      if not test:    
        cursor.execute("UPDATE migration_mail SET migrated = %s WHERE arsys_key = %s",[numlot,arsyskey])

    return actualsize

if __name__ == '__main__':
    main()
