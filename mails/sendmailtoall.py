#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ========================================================================
# Envoie un mail à tous les utilisateurs bluemind sur leur mail par défaut
#
# ========================================================================

# Pour utiliser les services soap de teclib
from suds import WebFault
from suds.client import Client

# Import the email modules we'll need
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formatdate
from email.utils import make_msgid
from email import charset

from os.path import basename

import sys


reload(sys);
sys.setdefaultencoding("utf8")

def main():
    url = 'https://bluemind.atd-quartmonde.org/soap/proxy?wsdl'
    c = Client(url);

    token = c.service.login('admin@atd-quartmonde.org', '4e5342f4-2a55-4068-8d83-e84075af4f0c', 'sendmail2all')
    print token

    try:
        #print c
        uq = c.factory.create('userQuery')
        # commenter cette ligne pour envoyer à tout le monde
        #uq.email='nicolas.duclos@atd-quartmonde.org'
        uq.archived=False
        #print uq
        users = c.service.findUsers(token,uq)
        #print users

        print 'Connexion au smtp...'
        s = smtplib.SMTP('smtp.atd-quartmonde.org', 587, None, 20)
        #s.set_debuglevel(1)
        s.starttls()
        s.login('envoi@atd-quartmonde.org','xwmhYRBvdJGC')

        for user in users:
            settings = c.service.getUserSettings(token,user.id)

            # TODO déterminer la langue également avec le nom de domaine par défaut.
            # Recherche de la langue
            if hasattr(settings.settings, 'entry') :
                for entry_setting in settings.settings.entry :
                    if entry_setting.key == 'set_lang':
                        lang = entry_setting.value
                        break
            if not lang:
                lang = 'fr'

            # un mystérieux bmhiddensysadmin n'a pas user.email.entry et on n'en a pas besoin
            # Et on envoie le mail que por les francophones.
            if hasattr(user.emails, 'entry') and lang == 'fr':
                for email in user.emails.entry:
                    if email.value.default == True:
                      #print email
                      # TODO mettre subject et body plus haut (dans sendthemail ?) si même message à tout le monde.
                      print "envoi du mail à {0}".format(email.value.email)
                      #subject = 'Interruption des mails lundi matin'
                      subject = 'Conseil aux aventuriers du net'
                      # body = "Bonjour,\n\nUne interruption des mails est prévue lundi matin à partir de 8h00 et devrait durer 2 heures. Cette interruption permettra de corriger un dysfonctionnement du système. Aucun mail ne sera perdu.\n\nCordialement\nL'équipe du support\nNicolas Duclos et Vincent Lucy"
                      body = """Bonjour,

Suite à une recrudescence de virus sur nos adresses ATD et à la perte définitive des fichiers de certain-e-s utilisatrices-eurs, nous avons identifié 6 points de vigilance, détaillés en pièce jointe, pour utiliser nos emails en toute sécurité :

1 - Ne jamais cliquer sur des pièces jointes ou des liens envoyés par des utilisateurs inconnus.

2 - Même si l'utilisateur est connu, réfléchir avant de cliquer : Est-ce logique qu'il y ait un fichier joint ? Ne jamais ouvrir un fichier joint si on a le moindre doute sur sa nature ou la raison de l'envoi.

3 - Faire des sauvegardes régulières.

4 - Mettre à jour régulièrement son système et ses logiciels.

5 - Télécharger les logiciels depuis leurs sites officiels.

6 - Ne pas utiliser de logiciels piratés.

Amicalement
Le comité de pilotage informatique
"""
                      body = body.format(email.value.email)
                      files = ['/home/nicolas/Documents/Informatique du mouvement/Mails BlueMind/Conseils_aux_aventuriers_du_net.pdf']

                      sendthemail(s,subject,body,[email.value.email],files)

        s.quit()
        print 'deconnexion du smtp !'

    except Exception, e:
        print e

    c.service.logout(token)
    print 'Logged out !'


def sendthemail(server,subject,body,recipients,files=None):
    """
    Construit le mail à envoyer
    """
    assert isinstance(recipients, list)
    assert isinstance(server, smtplib.SMTP)
    assert subject
    assert body

    charset.add_charset('utf-8', charset.QP, charset.QP)
    copies = [] #'support@atd-quartmonde.org']

    sender = 'support@atd-quartmonde.org'
    copyto = ', '.join(copies)
    sendto = ', '.join(recipients)
    msgsubj = Header(subject, 'utf-8')
    msgdate = formatdate()
    msgid = make_msgid()
    msgtext = MIMEText(body.encode('utf-8'), _charset='utf-8')

    if (files) :
        msg = MIMEMultipart()
        msg.attach(msgtext)

        for f in files or []:
            with open(f, "rb") as fil:
                msg.attach(MIMEApplication(
                    fil.read(),
                    Content_Disposition='attachment; filename="%s"' % basename(f),
                    Name=basename(f)
                ))

    else :
        msg = msgtext

    msg['Subject'] = msgsubj
    msg['From'] = sender
    msg['To'] = sendto
    msg['BCC'] = copyto
    msg['Date'] = msgdate
    msg['Message-ID'] = msgid

    server.sendmail(sender, recipients + copies, msg.as_string())


if __name__ == '__main__':
    main()
