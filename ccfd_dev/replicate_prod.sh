#!/bin/bash

# Script de réplication d'un site wordpress du serveur de prod pour le mettre ici sur le serveur de test avec une url de test.
# ARG1 : Le nom du fichier de config à utiliser pour cette réplication sans .conf et qui se trouve dans le répertoire config

source ./config/$1.conf

local_domain_path="/var/www/$local_domain/htdoc/"
local_domain_ls=$(ls -l /var/www/$local_domain/ | grep htdoc)
local_domain_owner=$(echo "$local_domain_ls" | awk '{print $3}')
local_domain_group=$(echo "$local_domain_ls" | awk '{print $4}')

echo ""
echo "Verification de la place disponible sur le filesystem"
df -h . 2> /dev/null
echo ""

echo "on se met dans le répertoire du domaine"
cd $local_domain_path || exit
echo "OK"

echo "Commit de tous les changements dans GIT si git il y a"
echo "Si vous ne voulez pas faire de commit : NE PAS ENREGISTRER LE FICHIER AVEC LE MESSAGE."
cd $git_base
sudo -u $local_domain_owner git add -A
sudo -u $local_domain_owner git commit
sudo -u $local_domain_owner git push
echo "OK"

cd $local_domain_path

echo "Récupération du mot de passe de la base du site de test"
local_db_pass=$(grep DB_PASSWORD wp-config.php |sed 's/^.*DB_PASSWORD'"'"'\s*,\s*'"'"'\([^'"'"']*\).*/\1/')
echo "OK"

echo "Connexion en SFTP au site de prod pour le récupérer"
echo "ATTENTION : La connexion n'est possible que par clé ssh !"
lftp -u $sftp_user, -p 22167 -e "set xfer:log true; mirror --allow-chown --parallel=50 --delete htdoc/. $local_domain_path; quit" sftp://48.48.48.167
echo "OK"

distant_db_name=$(grep DB_NAME wp-config.php |sed 's/^.*DB_NAME'"'"'\s*,\s*'"'"'\([^'"'"']*\).*/\1/')

echo "Les bonnes infos pour la connexion à la base de données par wordpress"
sed -i 's/^\(.*DB_PASSWORD'"'"'\s*,\s*'"'"'\)[^'"'"']*/\1'$local_db_pass'/' wp-config.php
echo "OK"

echo "Le bon domaine pour le network wordpress (s'il y en a un)"
sed -i 's/^\(.*DOMAIN_CURRENT_SITE'"'"'\s*,\s*'"'"'\)[^'"'"']*/\1'$local_domain'/' wp-config.php
echo "OK"

echo "Met les bonnes permissions sur les fichiers"
chown -R $local_domain_owner:$local_domain_group .
# chmod tous les dossiers qui ne sont pas déjà en 750
find . -type d ! -perm 750 -exec chmod 750 {} \;
# chmod tous les fichiers qui ne sont pas déjà en 640
find . -type f ! -perm 640 -exec chmod 640 {} \;
chmod 400 ./wp-content/mu-plugins/*
chmod 400 ./wp-config.php
echo "OK"

echo "import de la base de données"
echo "Remarque le nom et le user de la base doit être le même dans les deux environnement !!!"
db_user=$(grep DB_USER wp-config.php |sed 's/^.*DB_USER'"'"'\s*,\s*'"'"'\([^'"'"']*\).*/\1/')
db_name=$(grep DB_NAME wp-config.php |sed 's/^.*DB_NAME'"'"'\s*,\s*'"'"'\([^'"'"']*\).*/\1/')
dump_file=$distant_db_name.dump.sql
mysql -h localhost -u $db_user -p$local_db_pass  $db_name < $dump_file
echo "OK"

echo "Modification du domaine dans la base pour fonctionner avec le site de test"
# Le script est récupéré ici : https://interconnectit.com/products/search-and-replace-for-wordpress-databases/ et ici https://github.com/interconnectit/Search-Replace-DB
# Met le bon domaine
php /root/webtools/Search-Replace-DB-master/srdb.cli.php -p $local_db_pass -h localhost -n $db_name -u $db_user -s "$search_domain_regexp" -r "$replace_domain_regexp" -g
echo "OK"
echo "Execution des requêtes SQL spécifiques"
mysql -p$local_db_pass -h localhost -u $db_user $db_name -e "$mysql_commands"
echo "OK"

cd $git_base
git_dir=${PWD##*/}
cd ..
mv $git_dir ${git_dir}_tmp
sudo -u $local_domain_owner $git_clone $git_dir
cp -R ${git_dir}_tmp/. $git_dir/
cd $git_dir
#sudo -u $local_domain_owner git init
#sudo -u $local_domain_owner $git_add
#sudo -u $local_domain_owner git pull
sudo -u $local_domain_owner git add -A
sudo -u $local_domain_owner git commit

echo "OK. Import terminé. Site $local_domain répliqué en site de test."
echo "Si c'est tout bon pour git on peut faire un rm -rf ${git_base}_tmp"
echo ""
echo "$message_fin"
