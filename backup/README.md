#README

## Qu'est ce qu'il y a là dedans ?

Ce répertoire contient les scripts utilisés pour la sauvegarde par duplicity et vers hubic.
Ils ont été récupérés ici : http://www.yvangodard.me/backup-incremental-avec-duplicity-installation-et-scripts-periodiques/
Et sous git ici : https://github.com/yvangodard/Scripts-Utiles

Ces scripts fonctionnent avec Duplicity 0.6 après avoir installé https://github.com/yvangodard/python-cloudfiles-hubic.git pour utiliser duplicity avec hubic.

Voir également : http://gu1.aeroxteam.fr/2013/03/17/hubic-et-duplicity/ et https://github.com/Gu1/python-cloudfiles-hubic

## Comment l'installer ?

Pour installer les scripts, le plus simple est de copier ce qui est dans sbin dans /usr/local/sbin et ce qui est dans etc dans /etc :

```
#!shell
sudo cp sbin/* /usr/local/sbin
sudo chown root:root /usr/local/sbin/master*
sudo chmod 740 /usr/local/sbin/master*
sudo cp etc/master-backup.conf /etc/
sudo chown root:root /etc/master-backup.conf
sudo chmod 640 /etc/master-backup.conf
```

Il faut ensuite modifier /etc/master-backup.conf avec les identifiants et tout ce qu'il faut pour se connecter à hubic.

Voici une idée de dossiers à sauvegarder :

    /etc, mais en excluant :
        /etc/fstab
        /etc/network/interfaces
        /etc/lvm
    /var, mais en excluant :
        /var/lib/dpkg
        /var/lib/apt
        /var/cache/apt
        /var/backups
        /var/lib/aptitude
        /var/lib/mysql
        /var/run
    /home
    /usr/local
    /root

Une autre solution pour avoir les scripts versionnés :

```
#!shell
sudo ln -s sbin/master-backup-cleanup.sh /usr/local/sbin/master-backup-cleanup.sh
sudo ln -s sbin/master-backup-restore.sh /usr/local/sbin/master-backup-restore.sh
sudo ln -s sbin/master-backup-status.sh /usr/local/sbin/master-backup-status.sh
sudo ln -s sbin/master-backup-listing.sh /usr/local/sbin/master-backup-listing.sh
sudo ln -s sbin/master-backup.sh /usr/local/sbin/master-backup.sh
sudo ln -s sbin/master-backup-verify.sh /usr/local/sbin/master-backup-verify.sh
sudo chown root:root sbin/master*
sudo chmod 740 sbin/master*
sudo cp etc/master-backup.conf /etc/
sudo chown root:root /etc/master-backup.conf
sudo chmod 640 /etc/master-backup.conf
```

## Comment l'utiliser ?

Une fois la configuration faite dans master-backup.conf, voici les scripts et leur utilisation.

### Sauvegarder à la main
Le script est à programmer dans un cron, mais on peut le lancer à la main comme ça :
```
#!shell
master-backup.sh --no-email --incremental
```
-> A COMPLETER pour avoir les options de backup !


### Restaurer
Pour restaurer :
```
#!shell
master-backup-restore.sh [-f] [-t x[s|m|h|D]] chemin_vers_fichier_ou_dossier [chemin_à_restaurer]
```
Options :

-f : pour forcer la restauration si la cible existe

-t [délai] : pour restaurer la version dispo au délai spécifié (ex.: 1h pour une heure, 3D pour 3 jours)
Par défaut, la restauration se fait en place mais on peut indiquer un nouveau chemin pour la restauration.

Par exemple :
```
#!shell
master-backup-restore.sh -f -t 2D /etc/master-backup.conf /etc/master-backup.conf.old
```
Cette commande va forcer la restauration du fichier /etc/master-backup.conf d’il y a deux jours sous le nouveau nom /etc/master-backup.conf.old.

```
#!shell
master-backup-restore.sh -f -t 2D /
```
Cette commande va tout restaurer et écraser les fichiers existant s'il y en a.

```
#!shell
master-backup-restore.sh -t 2D / /home/nicolas/restoretmp
```
Cette commande va tout restaurer et mettre ça dans le répertoire /home/nicolas/restoretmp.

### Lister les fichiers, simuler une restauration, ...

Pour lister les fichiers sauvegardés :
```
#!shell
master-backup-listing.sh
```

Pour comparer ce qui a été sauvegarder avec ce qui est sur le pc :
```
#!shell
master-backup-verify.sh
```

Pour faire un cleanup duplicity :
```
#!shell
master-backup-cleanup.sh
```

Et pour voir le statut de la sauvegarde actuelle :
```
#!shell
master-backup-status.sh
```

### programmer les sauvegardes

Les scripts peuvent être appelé dans le cron comme ceci :
```
#!shell
# backup quotidien principal chaque jour à 01h15
15 1 * * * /usr/local/sbin/master-backup.sh --report-status   #backup duplicity Quotidien
 
# backup secondaire en forçant l'appel du fichier de config /etc/master-backup-2.conf
# Dans /etc/master-backup-2.conf FULLDELAY=1D, pour gérer dans la crontab manuellement l'alternance Full / Incrémental
# On force un backup full chaque samedi à 01h15
15 1 * * 6 /usr/local/sbin/master-backup.sh --conf  /etc/master-backup-2.conf #backup Secondaire (Full / Hebdo)
# On force un backup incrémental chaque jour sauf le samedi à 01h30
30 1 * * 0-5 /usr/local/sbin/master-backup.sh --conf  /etc/master-backup-2.conf --incremental #backup secondaire (Incrémental / Quotidien)
```
